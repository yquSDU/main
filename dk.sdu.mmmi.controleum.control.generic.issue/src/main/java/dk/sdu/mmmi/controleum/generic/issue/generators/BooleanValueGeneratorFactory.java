/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public class BooleanValueGeneratorFactory extends AbstractValueGeneratorFactory {

    private final static UUID id = UUID.fromString("662bfffa-30d9-4c23-9f21-371576b4c164");
    
    public BooleanValueGeneratorFactory() {
        super("Boolean", id);
    }
    
    
    
    @Override
    public BooleanValueGenerator createGenerator(Number lowerBoundary, Number upperBoundary, Number resolution)
    {
        return new BooleanValueGenerator();
    }

    @Override
    public Number parseString(String value) throws NumberFormatException {
        return Short.parseShort(value);
    }
}
