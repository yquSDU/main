/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;


import java.util.UUID;

/**
 *
 * @author ancla
 */
public class IntegerValueGeneratorFactory extends AbstractValueGeneratorFactory {
    
    private final static UUID id = UUID.fromString("d05d9664-83e8-4f5d-ac41-a7a7f5d716b");
    
    public IntegerValueGeneratorFactory()
    {
        super("Integer", id);
    }
    
    @Override
    public IntegerValueGenerator createGenerator(Number lowerBoundary, Number upperBoundary, Number resolution) {
        return new IntegerValueGenerator(lowerBoundary.intValue(), upperBoundary.intValue(), resolution.intValue());
    }

    @Override
    public Number parseString(String value) throws NumberFormatException {
        return Integer.parseInt(value);
    }


}
