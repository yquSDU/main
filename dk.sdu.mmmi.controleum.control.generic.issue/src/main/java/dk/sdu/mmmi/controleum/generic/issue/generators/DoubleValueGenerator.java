/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;


/**
 *
 * @author ancla
 */
public class DoubleValueGenerator implements IValueGenerator {
    private final Double upperBoundary;
    private final Double lowerBoundary;
    private final Double resolution;
    
    
    public DoubleValueGenerator(Double lowerBoundary, Double upperBoundary, Double resolution) {
        this.upperBoundary = upperBoundary == null ? Double.MAX_VALUE : upperBoundary;
        this.lowerBoundary = lowerBoundary == null ? (-1.0 * Double.MAX_VALUE) : lowerBoundary;
        this.resolution = resolution == null ? Double.MIN_VALUE : resolution;
    }
    
    @Override
    public Double generateRandomValue()
    {
       if(resolution > 0.0)
       {
        return Math.round((Math.random() * (getUpperBoundary() - getLowerBoundary())) + getLowerBoundary()/resolution) * resolution;
       }
       else
       {
           return (Math.random() * (getUpperBoundary() - getLowerBoundary())) + getLowerBoundary();
       }
    }

    /**
     * @return the upperBoundary
     */
    @Override
    public Double getUpperBoundary() {
        return upperBoundary;
    }

    /**
     * @return the lowerBoundary
     */
    @Override
    public Double getLowerBoundary() {
        return lowerBoundary;
    }

    /**
     * @return the resolution
     */
    @Override
    public Double getResolution() {
        return resolution;
    }
}
