/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public class FloatValueGeneratorFactory extends AbstractValueGeneratorFactory {
    
    private final static UUID id = UUID.fromString("3979ec2a-78d5-4797-b91e-2985f5940d6a");
    
    public FloatValueGeneratorFactory() {
        super("Float", id);
    }
    
    @Override
    public FloatValueGenerator createGenerator(Number lowerBoundary, Number upperBoundary, Number resolution) {
        return new FloatValueGenerator(lowerBoundary.floatValue(), upperBoundary.floatValue(), resolution.floatValue());
    }

    @Override
    public Number parseString(String value) throws NumberFormatException {
        return Float.parseFloat(value);
    }

}
