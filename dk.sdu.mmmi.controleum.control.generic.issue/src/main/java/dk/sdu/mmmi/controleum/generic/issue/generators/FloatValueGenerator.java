/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;



/**
 *
 * @author ancla
 */
public class FloatValueGenerator implements IValueGenerator {
    private final Float upperBoundary;
    private final Float lowerBoundary;
    private final Float resolution;
    
    
    public FloatValueGenerator(Float lowerBoundary, Float upperBoundary, Float resolution) {
        this.upperBoundary = upperBoundary == null ? Float.MAX_VALUE : upperBoundary;
        this.lowerBoundary = lowerBoundary == null ? (-1.0F * Float.MAX_VALUE) : lowerBoundary;
        this.resolution = resolution == null ? Float.MIN_VALUE : resolution;
    }
    
    @Override
    public Float generateRandomValue()
    {
       //return ((float)Math.random() * (getUpperBoundary() - getLowerBoundary())) + getLowerBoundary();
        if(resolution > 0.0)
       {
            return Math.round((Math.random() * (getUpperBoundary() - getLowerBoundary())) + getLowerBoundary()/resolution) * resolution;
       }
       else
       {
          return ((float)Math.random() * (getUpperBoundary() - getLowerBoundary())) + getLowerBoundary();
       }
    }

    /**
     * @return the upperBoundary
     */
    @Override
    public Float getUpperBoundary() {
        return upperBoundary;
    }

    /**
     * @return the lowerBoundary
     */
    @Override
    public Float getLowerBoundary() {
        return lowerBoundary;
    }

    /**
     * @return the resolution
     */
    @Override
    public Float getResolution() {
        return resolution;
    }
}