/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.generic.issue;

import dk.sdu.mmmi.controleum.api.control.generic.ValueGeneratorTypes;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import dk.sdu.mmmi.controleum.api.control.generic.ValueEvent;
import dk.sdu.mmmi.controleum.api.control.generic.issue.GenericIssue;
import dk.sdu.mmmi.controleum.api.control.generic.issue.GenericIssueConfig;
import dk.sdu.mmmi.controleum.api.control.generic.issue.GenericIssueValueListener;
import java.util.BitSet;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.control.commons.AbstractIssue;
import dk.sdu.mmmi.controleum.control.generic.ControleumValueListImpl;
import java.net.URL;

/**
 *
 * @author ancla
 */
public class GenericIssueImpl extends AbstractIssue<ControleumValueList> implements GenericIssue {

    private final static Random R = new Random();
    private final ControleumContext cd;

    private GenericIssueConfig ic;
    private ValueGeneratorTypes generator;

    private final String uuidString;
    private final UUID issueId;

    public GenericIssueImpl(ControleumContext cd, GenericIssueConfig ic) {
        super(ic.getMapName(),cd);
        this.cd = cd;
        this.ic = ic;
        this.generator = ic.getGenerator();
        this.uuidString = ic.getId().toString();
        updateToConfig(ic);
            
        this.issueId = ic.getId();
    }
            
    
    @Override
    public UUID getIssueId()
    {
        return issueId;
    }


    public final void updateToConfig(GenericIssueConfig ic) {
        setName(ic.getName());
        this.generator = ic.getGenerator();
    }

    @Override
    public String getID() {
        return uuidString;
    }

    @Override
    public ControleumValueList getRandomValue(Date now) {
        ControleumValueList gs = new ControleumValueListImpl();
        gs.clear();
        for (int i = 0; i < ic.getTimeSlots(); i++) {
            gs.add(getRandomValue());
        }
        return gs;
    }

    private Number getRandomValue() {
        return generator.generateRandomValue(ic.getResolution(), ic.getUpperBoundary(), ic.getLowerBoundary());
    }

    @Override
    public ControleumValueList getMutatedValue(ControleumValueList v) {
        ControleumValueList ds = new ControleumValueListImpl(v);
        int randomIndex = R.nextInt(ds.size());
        Number oldValue = ds.get(randomIndex);
        Number newValue = getRandomValue();
        while (oldValue.equals(newValue)) {
            newValue = getRandomValue();
        }
        ds.set(randomIndex, newValue);
        return ds;
    }

    @Override
    public ControleumValueList copy(Date now, ControleumValueList schedule) {
        if (schedule != null) {
            return new ControleumValueListImpl(schedule);
        }
        return null;
    }

    @Override
    public ControleumValueList crossover(ControleumValueList schedule1, ControleumValueList schedule2) {
        ControleumValueList ds = new ControleumValueListImpl(schedule1);
        int scheme = R.nextInt(3);
        if (scheme == 0) {
            int splitStart = R.nextInt(schedule1.size());

            for (int i = splitStart; i < schedule1.size(); i++) {
                ds.set(i, schedule2.get(i));
            }
        }
        if (scheme == 1) {
            int splitStart = 4; //Size = 27, start = 26, slut = 1

            for (int i = splitStart; i < schedule1.size(); i++) {
                ds.set(i, schedule2.get(i));
            }

        }
        if (scheme == 2) {
            int index1 = R.nextInt(ds.size());
            int index2 = R.nextInt(ds.size());
            Number valueHolder = ds.get(index1);
            ds.set(index1, ds.get(index2));
            ds.set(index2, valueHolder);
        }
        return ds;
    }

    @Override
    public void doCommitValue(Date t, ControleumValueList result) {
        ValueEvent event = new ValueEvent(ic.getId(), result);
        for (GenericIssueValueListener l : context(cd).all(GenericIssueValueListener.class)) {
            l.notify(event);
        }
    }

    @Override
    public Number getUpperBound() {
        return ic.getUpperBoundary();
    }

    @Override
    public Number getLowerBound() {
        return ic.getLowerBoundary();
    }

    public int getSize() {
        return ic.getTimeSlots();
    }

    @Override
    public int bitSize() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BitSet asBitSet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setBitSet(BitSet bitset) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public URL getHtmlHelp() {
        URL url = getClass().getResource(getID() + ".html");
        return url;
    }

}
