/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.generic.issue.generators;

import java.util.UUID;

/**
 * The value generator factory is responsible for instantiating value generators.
 * A factory is needed, as this enables is to be shared in a context, regardless of the number of value generators actually created.
 * If no factory was present, a value generator would always have to be present in context to display the available types e.g. in a GUI.
 * @author ancla
 */
public abstract class AbstractValueGeneratorFactory {
    
    private final UUID id;
    private final String name;
    
    public AbstractValueGeneratorFactory(String name, UUID id)
    {
        this.id = id;
        this.name = name;
    }
    
    @Override
    public final String toString()
    {
        return name;
    }
    
    public final UUID getId()
    {
        return id;
    }
    
    public abstract IValueGenerator createGenerator(Number lowerBoundary, Number upperBoundary, Number resolution);
    
    public abstract Number parseString(String value) throws NumberFormatException;

}
