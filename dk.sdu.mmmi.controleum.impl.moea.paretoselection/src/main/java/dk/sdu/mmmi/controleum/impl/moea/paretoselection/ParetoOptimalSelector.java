package dk.sdu.mmmi.controleum.impl.moea.paretoselection;

import dk.sdu.mmmi.controleum.api.moea.ancla.IPopulationSelector;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.impl.moea.comperator.ParetoFitnessComparator;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author ancla
 */
public class ParetoOptimalSelector implements IPopulationSelector {

    public static final double EPS = 1e-10;
    private static final ParetoFitnessComparator PARETO_FITNESS = new ParetoFitnessComparator();

    @Override
    public boolean selectParetoFront(List<ISolution> paretoFront, List<ISolution> newPopulation) {
        boolean changed = false;
        for (ISolution s : newPopulation) {
            Map<Integer, List<ISolution>> sortByDomination = paretoFront.parallelStream().collect(Collectors.groupingBy(sR -> PARETO_FITNESS.compare(s, sR)));
            List<ISolution> newSolutionDominatedBy = sortByDomination.get(1);
            if (newSolutionDominatedBy == null) {
                paretoFront.add(s);
                changed = true;
                List<ISolution> solutionsDominatedByNewSolution = sortByDomination.get(-1);
                if (solutionsDominatedByNewSolution != null) {
                    paretoFront.removeAll(solutionsDominatedByNewSolution);
                }
            }
        }
        return changed;
    }

}
