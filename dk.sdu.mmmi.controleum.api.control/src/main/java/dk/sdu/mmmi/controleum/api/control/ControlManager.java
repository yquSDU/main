package dk.sdu.mmmi.controleum.api.control;

import com.decouplink.Disposable;
import dk.sdu.mmmi.controleum.common.units.Duration;

/**
 *
 * @author jcs
 */
public interface ControlManager extends Disposable {

    void stopControlOnce();

    void controlOnce();

    void startAutoControl();

    void stopAutoControl();

    void setSimulating(boolean isSimulating);

    void setDelayMS(long controlIntervalMS);

    Duration getDelay();

    boolean isSimulating();

    boolean isControlling();

    boolean isAutoRunning();

    boolean isSimCsvExport();

}
