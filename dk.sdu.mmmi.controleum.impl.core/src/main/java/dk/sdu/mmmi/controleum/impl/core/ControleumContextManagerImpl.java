package dk.sdu.mmmi.controleum.impl.core;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.generic.ValueGeneratorTypes;
import dk.sdu.mmmi.controleum.api.control.generic.issue.Boundary;
import dk.sdu.mmmi.controleum.api.control.generic.issue.GenericIssueConfig;
import dk.sdu.mmmi.controleum.api.core.ControleumDatabasePlugin;
import dk.sdu.mmmi.controleum.api.core.ControleumEvent;
import dk.sdu.mmmi.controleum.api.core.ControleumListener;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.db.DatabaseDataAccess;
import dk.sdu.mmmi.controleum.api.db.DatabaseProvider;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.common.config.ConcernConfig;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.lookup.ServiceProvider;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.core.ControleumContextManager;

@ServiceProvider(service = ControleumContextManager.class)
public class ControleumContextManagerImpl implements ControleumContextManager, LookupListener {

    private final Lookup.Result<ControleumDatabasePlugin> resultDb;
    private Collection<? extends ControleumDatabasePlugin> lastDb;
    private final Lookup.Result<ControleumPlugin> result;
    private Collection<? extends ControleumPlugin> last;
    private final Map<ControleumContext, DisposableList> disposables = new HashMap<>();
    private final DatabaseDataAccess dba;
    private final List<ControleumContext> domains;
    private final Boundary b = new Boundary(9, 0, 1);     //qqq  Boundary(Number upperBoundary, Number lowerBoundary, Number resolution) 
    private final GenericIssueConfig ic = new GenericIssueConfig("qIssueName", 7, 10, b, ValueGeneratorTypes.INTEGER);    //qqq public GenericIssueConfig(String name, int timeSlots, long timeSpan, Boundary boundary, ValueGeneratorTypes generator)

    public ControleumContextManagerImpl() {

        // Lookup stuff
        this.resultDb = Lookup.getDefault().lookupResult(ControleumDatabasePlugin.class);
        this.resultDb.addLookupListener(this);
        this.lastDb = resultDb.allInstances();

        this.result = Lookup.getDefault().lookupResult(ControleumPlugin.class);
        this.result.addLookupListener(this);

        this.last = result.allInstances();

        this.domains = new ArrayList<>();

        this.dba = Lookup.getDefault().lookup(DatabaseProvider.class).getDataAccess().getDestination();

    }

    @Override
    public void loadContexts() {
        // Load a single context. If none exists, create one.
        List<ContextDataAccess> contexts = dba.selectContexts();

        if (contexts.isEmpty()) {
            createContext("Default Context");
        } else {
            for (ContextDataAccess cda : contexts) {
                newControlDomain(cda);
            }
        }
    }

    @Override
    public ControleumContext createContext(String name) {
        return newControlDomain(dba.createContext(name));
    }

    @Override
    public void deleteContext(ControleumContext g) {

        dba.deleteContext(g.getID());
        domains.remove(g);

        // Dispose context
        for (Disposable d : context(g).all(Disposable.class)) {
            d.dispose();
        }
        DisposableList domainDisposables = disposables.get(g);
        domainDisposables.dispose();
        disposables.remove(g);
        Collection c = context(g).all(Object.class);
        for (ControleumListener l : context(g).all(ControleumListener.class)) {
            l.onControlDomainDeleted(new ControleumEvent(g));
        }

    }

    @Override
    public ControleumContext getContext(int id) {
        // Find existing Control Domains
        for (ControleumContext controlDomain : domains) {
            if (controlDomain.getID() == id) {
                return controlDomain;
            }
        }
        return null;
    }

    @Override
    public ControleumContext getContext(String name) {
        // Find existing Control Domains
        for (ControleumContext controlDomain : domains) {
            if (controlDomain.getName().equals(name)) {
                return controlDomain;
            }
        }
        return null;
    }

    //TODO should a list of all control domains ever be exposed?
    @Override
    public List<ControleumContext> getContexts() {
        return this.domains;
    }

    @Override
    public void renameContext(ControleumContext g, String name) {
        //TODO: What is going on here? Why is a ControlDomain being parsed as parameter, only to get overwritten?
        int id = g.getID();

        g = getContext(id);
        g.setName(name);

        // Update DB
        dba.updateContextName(id, name);

        // Notify listeners
        for (ControleumListener l : context(g).all(ControleumListener.class)) {
            l.onControlDomainUpdated(new ControleumEvent(g));
        }
    }

    private void onPluginAdded(ControleumPlugin gp) {
        for (ControleumContext g : domains) {
            disposables.get(g).add(gp.create(g, ic.getId()));       //qqq add ic.getId()

            // Notify listeners.
            for (ControleumListener l : context(g).all(ControleumListener.class)) {
                l.onControlDomainUpdated(new ControleumEvent(g));
            }
        }
    }

    @Override
    public void resultChanged(LookupEvent ev) {
        //TODO Cannot load database afterwards for now.
        Collection<? extends ControleumPlugin> now = result.allInstances();

        // Added plugins.
        for (ControleumPlugin gp : added(last, now)) {
            onPluginAdded(gp);
        }

        last = now;
    }

    private Collection<? extends ControleumPlugin> added(Collection<? extends ControleumPlugin> v1, Collection<? extends ControleumPlugin> v2) {

        Collection<ControleumPlugin> r = new ArrayList<>();

        for (ControleumPlugin gp : v2) {        //qqq just add non-added Plugin to the "now" ControleumPlugin
            if (!v1.contains(gp)) {
                r.add(gp);
            }
        }
        return r;
    }

    private ControleumContext newControlDomain(ContextDataAccess cda) {
        ControleumContext g = new ControleumContextImpl(cda.getName(), cda.getID());
        disposables.put(g, new DisposableList());
        context(g).add(ContextDataAccess.class, cda);

        //Instantiate each context specific database implementation
        lastDb.stream().forEach((db) -> {
            disposables.get(g).add(db.create(g));
        });
        //Instantiate each ControleumPlugin with this context.
        for (ControleumPlugin gp : last) {
            disposables.get(g).add(gp.create(g, ic.getId()));       //qqq add ic.getId()
        }
        domains.add(g);

        // Notify listeners.
        for (ControleumListener l : context(g).all(ControleumListener.class)) {
            l.onControlDomainAdded(new ControleumEvent(g));
        }
        return g;
    }

    @Override
    public void dispose() {
        domains.clear();
    }

    @Override
    public void loadConcernConfig(ControleumContext domain, Concern concern) {
        ContextDataAccess cda = context(domain).one(ContextDataAccess.class);

        ConcernConfig cc = cda.getConcernConfiguration(concern.getID());
        if (cc != null) {
            concern.setEnabled(cc.isEnabled(), false);
            concern.setPriority(cc.getPriority(), false);
        }
    }

    @Override
    public void saveConcernConfig(ControleumContext domain, Concern concern) {
        ContextDataAccess cda = context(domain).one(ContextDataAccess.class);
        ConcernConfig cc = new ConcernConfig.Builder().setEnabled(concern.isEnabled())
                .setPriority(concern.getPriority()).build();

        // Store data in database.
        cda.setConcernConfiguration(concern.getID(), cc);
    }
}
