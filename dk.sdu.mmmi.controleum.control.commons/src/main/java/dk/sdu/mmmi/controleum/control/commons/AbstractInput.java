package dk.sdu.mmmi.controleum.control.commons;

import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Value;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jcs
 */
public abstract class AbstractInput<V> implements Input<V> {

    private final List<Listener> listeners = new ArrayList<>();
    private String name;
    private final Value.Type type;
    private V value;

    public AbstractInput(String name) {
        this(name, Value.Type.REAL);
    }

    public AbstractInput(String name, Value.Type type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String getID() {
        return getClass().getName();
    }

    /*
     * Value interface
     */
    @Override
    public void addListener(Listener l) {
        listeners.add(l);
    }

    @Override
    public void removeListener(Listener l) {
        listeners.remove(l);
    }

    @Override
    public void fireValueChanged() {
        // Copy array to prevent Concurrent Modification Exception
        for (Listener l : new ArrayList<>(listeners)) {
            l.onValueChanged(this);
        }
    }

    @Override
    public final Type getType() {
        return type;
    }

    @Override
    public final V getValue() {
        return value;
    }

    @Override
    public void setValue(V value) {
        this.value = value;
        fireValueChanged();
    }

    @Override
    public final String toString() {
        return String.format("%s = <b>%s</b>", getName(), getValue());
    }

    /*
     * Element interface
     */
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public URL getHtmlHelp() {
        URL url = getClass().getResource(getID() + ".html");
        return url;
    }
}
