package dk.sdu.mmmi.controleum.control.commons.series;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeSeries;
import dk.sdu.mmmi.controleum.api.data.DoubleTimeValue;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.common.results.ConcernNegotiationResult;
import dk.sdu.mmmi.controleum.common.units.Sample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 *
 * @author jcs
 */
public class ConcernFitnessTimeSeries extends DoubleTimeSeries {

    private final ControleumContext g;
    private final Concern concern;
    private boolean isNormalized = true;

    public ConcernFitnessTimeSeries(ControleumContext g, Concern concern) {
        this.g = g;
        this.concern = concern;
        setName(concern.getName());
        setUnit("[Satisfaction]");
    }

    @Override
    public String getID() {
        return concern.getClass().getName();
    }

    @Override
    public List<DoubleTimeValue> getValues(Date from, Date to) throws Exception {

        List<DoubleTimeValue> concernData = new ArrayList<>();

        ContextDataAccess db = context(g).one(ContextDataAccess.class);

        Double minFitness = db.selectConcernMinFitness(getID(), from, to);
        Double maxFitness = db.selectConcernMaxFitness(getID(), from, to);

        for (Sample<ConcernNegotiationResult> s : db.selectConcernNegotiationResult(getID(), from, to)) {

            double fitness = s.getSample().getCost();
            Date time = s.getTimestamp();

            DoubleTimeValue v = new DoubleTimeValue(time, fitness);
            if (isNormalized) {
                v = normalize(minFitness, maxFitness, fitness, v, time);
            }

            concernData.add(v);
        }

        return concernData;
    }

//    public void setNormalized(boolean normalized) {
//        this.isNormalized = normalized;
//    }
    private DoubleTimeValue normalize(Double minFitness, Double maxFitness, double fitness, DoubleTimeValue v, Date time) {
        // normalize data
        if (minFitness != null && maxFitness != null
                && minFitness < maxFitness) {

            Double normalizedValue = (fitness - minFitness) / (maxFitness - minFitness);
            v = new DoubleTimeValue(time, normalizedValue);
        }
        return v;
    }
}
