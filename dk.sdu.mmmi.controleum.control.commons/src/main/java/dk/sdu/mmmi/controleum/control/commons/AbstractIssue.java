/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.control.commons;

import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.moea.Issue;
import dk.sdu.mmmi.controleum.api.moea.IssueObserver;
import dk.sdu.mmmi.controleum.api.moea.Value.Listener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public abstract class AbstractIssue<V> implements Issue<V> {

    private boolean enabled = true;

    public final ControleumContext domain;
    private boolean dbInitialized = false;

    private String name;
    private final Type type;
    private V value;
    private final UUID id;

    private final List<Listener> listeners = new ArrayList<>();
    private final List<IssueObserver<V>> observers = new ArrayList<>();

    private boolean mutable = true;

    public AbstractIssue(String name, ControleumContext domain) {
        this(name, domain, Type.REAL);
    }

    public AbstractIssue(String name, ControleumContext domain, Type t) {
        this(name, domain, t, UUID.randomUUID());
    }

    public AbstractIssue(String name, ControleumContext domain, Type t, UUID id) {
        this.name = name;
        this.domain = domain;
        this.type = t;
        this.id = id;
    }

    //
    // Issue interface
    //
    @Override
    public V crossover(V v1, V v2) {
        return v2;
    }

    @Override
    public abstract V copy(Date time, V obj);

    @Override
    public void doCommitValue(Date t, V result) {
        for (IssueObserver observer : observers) {
            observer.update(result);
        }
    }

    @Override
    public void addObserver(IssueObserver<V> observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(IssueObserver<V> observer) {
        observers.remove(observer);
    }

    //
    // Value interface
    //
    @Override
    public void addListener(Listener l) {
        listeners.add(l);
    }

    @Override
    public void removeListener(Listener l) {
        listeners.remove(l);
    }

    @Override
    public void fireValueChanged() {
        // Copy array to prevent Concurrent Modification Exception
        for (Listener l : new ArrayList<>(listeners)) {
            l.onValueChanged(this);
        }
    }

    @Override
    public final Type getType() {
        return type;
    }

    @Override
    public final V getValue() {
        return value;
    }

    @Override
    public void setValue(V value) {
        this.value = value;
        fireValueChanged();
    }

    @Override
    public final String toString() {
        //qqq return String.format("%s = <b>%s</b>", getName(), getValue());
        //TODO //qqq prevent two "Light plan = "
        if (getValue() != null) {
            return String.format("%s = <b>%s</b>", getName(), getValue());
        } else {
            return String.format("<b>%s</b>", getValue());
        }
    }

    @Override
    public String getID() {
        return id.toString();
    }

    @Override
    public URL getHtmlHelp() {
        URL url = getClass().getResource(getID() + ".html");
        return url;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void markImmutable() {
        this.mutable = false;
        System.out.println(this.getName() + " of " + domain.getName() + " became immutable");
    }

    @Override
    public void markMutable() {
        this.mutable = true;
    }

    @Override
    public boolean isMutable() {
        return this.mutable;
    }

}
