package dk.sdu.mmmi.controleum.control.commons;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.db.ContextDataAccess;
import dk.sdu.mmmi.controleum.api.moea.Output;
import java.net.URL;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.common.config.OutputConfig;

/**
 * A common base class for control domain concerns. The main feature of this
 * class is that its add persistence for "concern enabled/disabled".
 *
 * @author mrj, jcs
 */
public abstract class AbstractOutput<V> implements Output<V> {

    private boolean enabled = true;

    public final ControleumContext domain;
    private boolean dbInitialized = false;

    private String name;
    private V value;

    public AbstractOutput(String name, ControleumContext domain) {
        this.name = name;
        this.domain = domain;
    }

    @Override
    public synchronized void setEnabled(boolean enabled, boolean fireEvent) {

        // Set DB.
        ContextDataAccess cda = getData();
        if (cda != null) {
            OutputConfig cc = new OutputConfig();
            cc.setEnabled(enabled);
            cda.setOutputConfiguration(getID(), cc);

            dbInitialized = true;
        }

        // Set
        this.enabled = enabled;
    }

    @Override
    public synchronized boolean isEnabled() {

        if (!dbInitialized) {
            ContextDataAccess cda = getData();
            if (cda != null) {
                OutputConfig cc = cda.getOutputConfiguration(getID());
                if (cc != null) {
                    setEnabled(cc.isEnabled(), false);
                }
            }

            dbInitialized = true;
        }

        return this.enabled;
    }

    private ContextDataAccess getData() {
        return context(domain).one(ContextDataAccess.class);
    }
    @Override
    public final String toString() {
        return String.format("%s = <b>%s</b>", getName(), getValue());
    }


    @Override
    public String getID() {
        return getClass().getName();
    }
        
    @Override
    public V getValue()
    {
        return value;
    }
    
    //
    // Element interface
    //
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public URL getHtmlHelp() {
        URL url = getClass().getResource(getID() + ".html");
        return url;
    }

}
