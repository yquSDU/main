package dk.sdu.mmmi.controleum.api.data;

import java.util.Date;

/**
 *
 * @author jcs
 */
public interface TimeValue<V> {

    Date getTime();

    V getValue();
}
