package dk.sdu.mmmi.controleum.api.data;

import java.util.Date;
import java.util.List;

/**
 *
 * @author jcs
 */
public interface TimeSeries {

    String getID();

    String getName();

    String getUnit();

    long getPeriod();

    void setName(String name);

    void setUnit(String unit);

    void setMaxPeriodLenghtMS(long ms);
    /**
     * @return the max. period allowed between data points.
     * If a period is above max then data should not be connected.
     */
    long getMaxPeriodLenghtMS();

    /**
     * Implement this method to provide data. It is important the returned list
     * is sorted in ascending order. See DoubleTimeValue.ASCENDING_ORDER.
     */
    List<? extends TimeValue> getValues(Date from, Date to) throws Exception;
}
