package dk.sdu.mmmi.controleum.api.data;

import org.netbeans.api.progress.ProgressHandle;
import java.util.Queue;
import java.util.List;
import java.util.Collection;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import static com.decouplink.Utilities.*;
import java.text.SimpleDateFormat;

/**
 * Export all data series that exists in some context.
 *
 * @author mrj
 */
public class CSVExporter {


    private final static String SEP = ",";
    private final Object context;
    private final Date start, end;

    public CSVExporter(Object context) {
        this.context = context;
        Calendar c = Calendar.getInstance();
        // int year, int month, int date, int hourOfDay, int minute, int second
        c.set(1970, Calendar.JANUARY, 0, 0, 0,0);
        this.start = c.getTime();
        this.end = new Date();
    }

    public CSVExporter(Object context, Date start, Date end) {
        this.context = context;
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public void export(File file, ProgressHandle h) throws Exception {
        try (PrintWriter pw = new PrintWriter(file)) {
            write(pw, h);
        }
    }

    //
    // Private methods related to progress management.
    //
    private void start(ProgressHandle h, int size) {
        if (h != null) {
            h.start(size);
        }
    }

    private void finish(ProgressHandle h) {
        if (h != null) {
            h.finish();
        }
    }

    private void progress(ProgressHandle h, int progress) {
        if (h != null) {
            h.progress(progress);
        }
    }

    private void progress(ProgressHandle h, String msg) {
        if (h != null) {
            h.progress(msg);
        }
    }

    private void write(PrintWriter pw, ProgressHandle h) throws Exception {

        try {
            Collection<? extends DoubleTimeSeries> series =
                    context(context).all(DoubleTimeSeries.class);

            // Titles.
            start(h, 100);
            writeTitles(pw, series);

            // Data.
            Calendar itr = Calendar.getInstance();
            itr.setTime(getStart());

            long startMS = getStart().getTime();
            long totalMS = getEnd().getTime() - startMS;

            while (itr.getTime().before(getEnd())) {
                Date a = itr.getTime();
                itr.add(Calendar.DATE, 1);
                Date b = itr.getTime();
                writeData(series, a, b, pw);
                double currentMS = (double) b.getTime() - startMS;
                double pct = (currentMS / totalMS) * 100;
                progress(h, (int) pct);
            }

        } finally {
            finish(h);
        }
    }

    private void writeTitles(PrintWriter pw, Collection<? extends DoubleTimeSeries> series) {
        pw.print("TimeMS");
        pw.print(SEP);
        pw.print("Time");
        for (DoubleTimeSeries s : series) {
            pw.print(SEP + s.getName().trim().replace(" ", "_").replace("/", "_").replace("/?", "").replace(".", ""));
        }
        pw.println();
    }

    private void writeData(
            Collection<? extends DoubleTimeSeries> series,
            Date start, Date end, PrintWriter pw) throws Exception {

        DataSet data = new DataSet();

        // Populate DataSet.
        for (DoubleTimeSeries s : series) {
            data.addData(s.getValues(start, end));
        }

        // Write data using DataSet.
        Date i;
        while ((i = data.getEarlistDate()) != null) {
            data.printRow(pw, i);
        }
    }

    private static class DataSet {
        private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<Queue<DoubleTimeValue>> data = new ArrayList<>();

        public void addData(List<DoubleTimeValue> series) {
            data.add(new LinkedList<>(series));
        }

        public Date getEarlistDate() {
            Date earliest = null;
            for (Queue<DoubleTimeValue> q : data) {
                DoubleTimeValue v = q.peek();
                if (v != null) {
                    if (earliest == null || v.getTime().before(earliest)) {
                        earliest = v.getTime();
                    }
                }
            }
            return earliest;
        }

        public void printRow(PrintWriter pw, Date ts) {
            pw.print(ts.getTime());
            pw.print(SEP);
            pw.print(df.format(ts));
            for (Queue<DoubleTimeValue> q : data) {
                DoubleTimeValue v = q.peek();
                if (v != null && !v.getTime().after(ts)) {
                    pw.print(SEP + String.format("%.2f", v.getValue()));
                    q.poll();
                } else {
                    pw.print(SEP);
                }
            }
            pw.println();
        }
    }
}
