package dk.sdu.mmmi.controleum.api.db;

import com.decouplink.Shareable;
import dk.sdu.mmmi.controleum.common.config.ConcernConfig;
import dk.sdu.mmmi.controleum.common.config.ControlConfig;
import dk.sdu.mmmi.controleum.common.config.IssueConfig;
import dk.sdu.mmmi.controleum.common.config.OutputConfig;
import dk.sdu.mmmi.controleum.common.results.ConcernNegotiationResult;
import dk.sdu.mmmi.controleum.common.results.NegotiationResult;
import dk.sdu.mmmi.controleum.common.units.Sample;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author jcs,ac Generic DB API for Controleum.
 */
@Shareable
public interface ContextDataAccess {

    int getID();

    String getName();

    ControlConfig getControlConfiguration();

    ConcernConfig getConcernConfiguration(String id);
    
    IssueConfig getIssueConfiguration(String id);

    OutputConfig getOutputConfiguration(String id);

    void setControlConfiguration(ControlConfig cfg);

    void setConcernConfiguration(String id, ConcernConfig cfg);

    void setOutputConfiguration(String id, OutputConfig cfg);
    
    void setIssueConfiguration(String id, IssueConfig cfg);

    Double selectConcernMinFitness(String concernID, Date fr, Date to);

    Double selectConcernMaxFitness(String concernID, Date fr, Date to);

    Sample<ConcernNegotiationResult> selectConcernNegotiationResult(String concernID, Date t);

    void insertConcernNegotiationResult(Sample<ConcernNegotiationResult> s);

    List<Sample<ConcernNegotiationResult>> selectConcernNegotiationResult(String concernID, Date from, Date to);

    Map<Date, List<ConcernNegotiationResult>> selectConcernsNegotiationResult(Date from, Date to);

    void insertNegotiationResult(Sample<NegotiationResult> s);
}
