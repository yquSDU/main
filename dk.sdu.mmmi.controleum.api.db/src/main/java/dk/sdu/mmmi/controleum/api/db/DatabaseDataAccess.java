package dk.sdu.mmmi.controleum.api.db;

import com.decouplink.Shareable;
import java.util.List;

/**
 * @author mrj
 */
@Shareable
public interface DatabaseDataAccess {

    void deleteContext(int ID);

    ContextDataAccess createContext(String name);

    List<ContextDataAccess> selectContexts();

    void updateContextName(int id, String name);
}
