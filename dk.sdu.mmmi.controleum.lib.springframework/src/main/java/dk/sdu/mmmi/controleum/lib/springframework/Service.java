package dk.sdu.mmmi.controleum.lib.springframework;

import org.openide.modules.ModuleInstall;
import org.openide.util.Lookup;
import org.springframework.context.ApplicationContext;

public class Service extends ModuleInstall {

    private static ApplicationContext context;

    @Override
    public void restored() {
        context = SpringAndLookup.create(Lookup.getDefault(), "java.extensions");
    }

    public static ApplicationContext getServiceContext() {
        return context;
    }
}
