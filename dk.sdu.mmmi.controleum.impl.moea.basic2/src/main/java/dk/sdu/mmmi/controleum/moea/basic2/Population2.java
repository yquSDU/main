package dk.sdu.mmmi.controleum.moea.basic2;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.ancla.IParetoFront;
import dk.sdu.mmmi.controleum.api.moea.ancla.IPopulation;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphSolutionSelector;
import dk.sdu.mmmi.controleum.api.moea.graph.IGraph;
import dk.sdu.mmmi.controleum.api.moea.graph.Selector;
import dk.sdu.mmmi.controleum.impl.moea.comperator.ParetoFitnessComparator;
import dk.sdu.mmmi.controleum.impl.moea.graph.GraphBasedBestSolutionSelection;
import dk.sdu.mmmi.controleum.impl.moea.graph.GraphBasedFinalSolutionSelection;
import dk.sdu.mmmi.controleum.impl.moea.paretofront.ParetoFront;
import dk.sdu.mmmi.controleum.impl.moea.solution.Option;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.ApproximatedFairnessSelectorI;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.MaximizeSocialWelfareSelector;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.api.moea.graph.IGraphPopulation;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;
import dk.sdu.mmmi.controleum.impl.moea.comperator.PrioritizedFitnessComparator;
import javolution.util.FastTable;

import java.util.*;

/**
 *
 * @author mrj & jcs
 */
class Population2 implements IGraphPopulation {

    //private final Object problem;
    /**
     * Pareto front
     */
    public static final double EPS = 1e-10;

    private IParetoFront pop; // best first

    List<ISolution> candidatePopulation = new ArrayList<>();

    private boolean evolved = false;

    private final List<Concern> concerns;

    private final int localPopSize = 500;
    /*
     * Final selection
     */
    private final ISolutionSelector selector = new MaximizeSocialWelfareSelector();

    private final SearchConfiguration cfg;

    private final static Random R = new Random();
    private static final ParetoFitnessComparator PARETO_FITNESS = new ParetoFitnessComparator();

    //qqq ancl for graph
    private Selector s;
    private IGraph graph;
    private final GraphSolutionSelector selectorBest = new GraphBasedBestSolutionSelection();
    private final GraphSolutionSelector selectorFinal = new GraphBasedFinalSolutionSelection();
    /////////////////////////////////////////

    /**
     * Create initial population.
     */
    public Population2(Object problem, Date time, IPopulation reuse, SearchConfiguration cfg, List<Concern> concerns) {
        this.cfg = cfg;
        this.pop = new ParetoFront(new FastTable<>(), cfg);
        this.concerns = concerns;
        if (reuse != null) {
            for (ISolution oldSolution : reuse.getPopulation()) {
                pop.add((ISolution) oldSolution.copy(problem, time));
            }
        } else {
            pop.addAll(generateRandomSolutions(problem, time));
        }
        evolved = true;
    }

    private List<ISolution> generateRandomSolutions(Object problem, Date time) {
        // Use randomness
        List<ISolution> ranPop = new ArrayList<>();
        for (int i = 0; i < localPopSize; i++) {
            ranPop.add(Option.random(problem, time, getConcerns()));
        }
        return ranPop;
    }

    /**
     * Evolve new population.
     */
    private Population2(IPopulation old) {
        this.cfg = old.getSearchConfiguration();
        this.concerns = old.getConcerns();
        this.pop = old.getFront();
        //this.sortedPop = old.getSortedPop();
        // Determine number of tries for mutation and crossover
        candidatePopulation.clear();
        geneticOperation2();
        //System.out.println("C1 " +  c1 + " C2 " + c2);
        if (candidatePopulation.size() > 0) {
            evolved = pop.addAll(candidatePopulation);
        }

        //       System.out.println("Population2 size: " + pop.getPop().size());
    }

    private void geneticOperation2() {
        List<ISolution> paretoFrontCopy = pop.getCopy();
        for (int i = 0; i <= localPopSize; i++) {

            if (R.nextDouble() < cfg.getMutationRate()) {
                // Mutate.
                ISolution parent = randomElement(paretoFrontCopy);
                ISolution child = (ISolution) parent.mutate();
                if (checkDominate(child, parent)) {
                    candidatePopulation.add(child);
                }

            } else {
                // Crossover.
                ISolution parentA = randomElement(paretoFrontCopy);
                ISolution parentB = randomElement(paretoFrontCopy);
                ISolution child = (ISolution) parentA.crossover(parentB);
                if (checkDominate(child, parentA) && checkDominate(child, parentB)) {
                    candidatePopulation.add(child);
                }
            }
        }
    }

    /*
    * Experimental implementation of genetic operation which maps to the conventional way of doing crossover and mutation in GAs.
    * Seems to underperform with domain specific operators compared to the geneticOperation2 implementation (Jan's implementation).
     */
    private void geneticOperation() {
        int c1 = 0;
        List<ISolution> paretoFrontCopy = pop.getCopy();
        for (int i = 0; i < paretoFrontCopy.size() * cfg.getCrossoverRate(); i++) {
            c1++;
            ISolution parentA = randomElement(paretoFrontCopy);
            ISolution parentB = randomElement(paretoFrontCopy);
            ISolution child = (ISolution) parentA.crossover(parentB);
            if (checkDominate(child, parentA) && checkDominate(child, parentB)) {
                candidatePopulation.add(child);
            }
        }
        int c2 = 0;
        for (int i = 0; i < Math.ceil(paretoFrontCopy.size() * cfg.getMutationRate2()); i++) {
            // Mutate.
            c2++;
            ISolution parent = randomElement(paretoFrontCopy);
            ISolution child = (ISolution) parent.mutate();
            if (checkDominate(child, parent)) {
                candidatePopulation.add(child);
            }
        }
    }

    private boolean checkDominate(ISolution child, ISolution parent) {
        int flag = PARETO_FITNESS.compare(child, parent);
        if (flag > 0) {
            return false;
        } else {
            return !(NormalizationUtil.Distance(child, parent) < EPS);
        }
    }

    @Override
    public IPopulation evolve() {
        return new Population2(this);
    }

    /*
    * @deprecated
    * Use graph-based selection instead.
     */
    @Deprecated
    @Override
    public ISolution getBestSolution() {
        return selector.getSolution(pop.getCopy());
    }

    /*
    * @deprecated
    * Use graph-based selection instead.
     */
    @Deprecated
    @Override
    public ISolution getWorstSolution() {
        List<ISolution> paretoFrontCopy = pop.getCopy();
        paretoFrontCopy.sort(BY_PRIORITIZED_FITNESS);
        return paretoFrontCopy.get(paretoFrontCopy.size() - 1);
    }

    /*
    * @deprecated
    * Use graph-based selection instead.
     */
    @Deprecated
    @Override
    public ISolution getMeanSolution() {
        List<ISolution> paretoFrontCopy = pop.getCopy();
        paretoFrontCopy.sort(BY_PRIORITIZED_FITNESS);
        return paretoFrontCopy.get(paretoFrontCopy.size() / 2);
    }

    @Override
    public List<Double> getBestFitness() {
        return getBestSolution().getEvaluationValues();
    }

    @Override
    public List<Double> getWorstFitness() {
        return getWorstSolution().getEvaluationValues();
    }

    @Override
    public List<Double> getMeanFitness() {
        return getMeanSolution().getEvaluationValues();
    }

    @Override
    public double getFitnessVariance() {
        double sum = 0;
        List<ISolution> paretoFrontCopy = pop.getCopy();
        for (ISolution s : paretoFrontCopy) {
            double diff = NormalizationUtil.Distance(getMeanSolution(), s);
            sum += diff;
        }
        return sum / (double) paretoFrontCopy.size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("population = { ");
        sb.append("size : ");
        sb.append(getSize());
        sb.append(", ");
        sb.append("solutions : {\n");
        for (ISolution solution : pop.getCopy()) {
            sb.append(solution);
            sb.append(",\n");
        }
        sb.append("}");

        return sb.toString();
    }

    @Override
    public int getSize() {
        return pop.getCopy().size();
    }

    //
    // Misc.
    //
    private <T> T randomElement(List<T> list) {
        return list.get(R.nextInt(list.size()));
    }

    //
    // Comparators.
    //
    private final Comparator<ISolution> BY_PRIORITIZED_FITNESS = new PrioritizedFitnessComparator();

    @Override
    public List<ISolution> getPopulation() {
        return pop.getCopy();
    }

    @Override
    public SearchConfiguration getSearchConfiguration() {
        return cfg;
    }

    /**
     * @return the concerns
     */
    @Override
    public List<Concern> getConcerns() {
        return concerns;
    }

    /**
     * @return the evolved
     */
    @Override
    public boolean isEvolved() {
        return evolved;
    }

    @Override
    public IParetoFront getFront() {
        return pop;
    }

    @Override
    public ISolution getFairSolution() {
        ISolutionSelector fairSelector = new ApproximatedFairnessSelectorI();
        return fairSelector.getSolution(pop.getCopy());
    }

    @Override
    public ISolution getMaxSocialBenefitSolution() {
        ISolutionSelector maxBenefitSelector = new MaximizeSocialWelfareSelector();
        return maxBenefitSelector.getSolution(pop.getCopy());
    }

    @Override
    public ISolution getBestGraphSolution(Selector s) {     //qqq no use
        this.s = s;
        ISolution ss = selectorBest.getSolution(pop.getCopy(), s);
        this.graph = selectorBest.getGraph();
        return ss;
    }

    @Override
    public ISolution getFinalGraphSolution(Selector s) {        //qqq replace getBestSolution() for final selection
        this.s=s;
        ISolution ss=selectorFinal.getSolution(pop.getCopy(), s);   //qqq s == SOCIAL
        this.graph=selectorFinal.getGraph();
        return ss;
    }

    @Override
    public Selector getSelector() {
        return this.s;
    }

    @Override
    public Object getGraph() {
        return this.graph;
    }

    @Override
    public IGraphPopulation evolveAsGraphPop() {
        return new Population2(this);
    }
}
