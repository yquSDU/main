package dk.sdu.mmmi.controleum.moea.basic2;

import com.decouplink.Link;
import static com.decouplink.Utilities.context;

import dk.sdu.mmmi.controleum.api.moea.*;
import dk.sdu.mmmi.controleum.api.moea.ancla.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.SwingUtilities;

import dk.sdu.mmmi.controleum.api.moea.graph.GraphType;
import dk.sdu.mmmi.controleum.impl.moea.solution.Option;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import java.util.concurrent.atomic.AtomicBoolean;

import dk.sdu.mmmi.controleum.api.moea.graph.IGraphPopulation;
import dk.sdu.mmmi.controleum.api.moea.graph.IGraph;
import dk.sdu.mmmi.controleum.api.moea.graph.Selector;

/**
 * TODO: Make the algorithm parallel according to
 * http://www.heatonresearch.com/node/2955
 *
 * @author mrj & jcs
 */
public class SolverImpl2 implements ISolver {

    private final ControleumContext problem;
    private IPopulation solutionPop;
    private long lastConvergeMS;
    private int lastConvergenGen;
    private ISolution solution;
    private Link<SearchConfiguration> searchCfg;

    private int generation = 0;
    private int configurationSampleIndex = 0;
    private ArrayList configList;
    private int currentConfigIndex = 0;
    private Integer totalConfigSamples = 0;
    private boolean accumulatorNeeded = false;

    private AtomicBoolean terminated = new AtomicBoolean(false);

    // Aisha (integration of relative importance)
    private IGraphPopulation p;
    private List<Concern> concerns;
    private boolean inputFlag = true;
    private IGraph g;
    private List<Concern> rootConcerns;
    private Date date;
    private long durMS;
    private long startMS;

    /**
     *
     * @param context
     */
    public SolverImpl2(ControleumContext context) {
        this.problem = context;
        this.searchCfg = context(problem).add(SearchConfiguration.class, new SearchConfiguration.Builder().reproductionFactor(5).maxNegotiationTimeMS(1000L * 20).mutationRate(0.5).build());
    }

    /**
     *
     * @param context
     * @param cfg
     */
    public SolverImpl2(ControleumContext context, SearchConfiguration cfg) {
        this.problem = context;
        this.searchCfg = context(problem).add(SearchConfiguration.class, cfg);
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public SearchStatistics solve(Date t) {
        return solve(t, null, false);
    }

    /**
     *
     * @param t
     * @param h
     * @param useSwingThreadNotification
     * @return
     */
    @Override
    public SearchStatistics solve(Date t, final ProgressHandle h, boolean useSwingThreadNotification) {

        this.date = t;
        // fetching peri negotiation graph
        for (IGraph gIndex : context(problem).all(IGraph.class)) {
            if (gIndex.getGraphType().equals(GraphType.CONCERN)) {
                this.g = gIndex;
            }
        }

        // fetching root concerns
        this.rootConcerns = g.getRootVertex().getData();    //qqq ??? no use of "rootConcerns"

        for (Issue o : context(problem).all(Issue.class)) {
            if (!o.isMutable()) {
                o.markMutable();
                System.out.println(((ControleumContext) problem).getName() + "=" + o.getName());
            }
        }

        SearchStatistics ns;
        this.durMS = 0;
        generation = 0;
        this.startMS = 0;
        int lastChangedGeneration = 0;
        long lastChangedMS = 0;
        System.out.println("Starting Clustering Solver v. 1");
        try {
            start(h, getSearchCfg().getMaxGenerations());

            for (Input i : context(problem).all(Input.class)) {
                progress(h, String.format("Reading input %s ", i.getName()), generation);
                i.doUpdateValue(t);
            }
            for (AgentSetup i : context(problem).all(AgentSetup.class)) {
                progress(h, String.format("Running preprocessing in %s ", i.getName()), generation);
                i.doSetup();
            }
            Collection<? extends Concern> concernCollection = context(problem).all(Concern.class);
            List<Concern> concerns = new ArrayList<>(concernCollection);

            progress(h, "Initializing.");

            IGraphPopulation p = new Population2(problem, t, null, getSearchCfg(), concerns);

            startMS = new Date().getTime();

            progress(h, "Negotiating.", generation);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            while (true) {

                generation++;

                System.out.println(dateFormat.format(new Date()) + " " + problem.getName() + " generation: " + generation + " pop size:" + p.getSize());
                p = p.evolveAsGraphPop();
                //System.out.println(p.getBestSolution().getEvaluationResult(0) + " " + p.getBestSolution().getEvaluationResult(1));

                durMS = new Date().getTime() - startMS;
                if (p.isEvolved()) {
                    lastChangedGeneration = generation;
                    lastChangedMS = durMS;
                }

                if (generation >= getSearchCfg().getMaxGenerations()) {
                    String message = "Terminated due to generation limit.";
                    progress(h, message);
                    printDebug(message);
                    break;
                }

                if (startMS != 0 && durMS >= getSearchCfg().getMaxNegotiationTimeMS()) {
                    String message = "Terminated due to time limit after " + generation + " generations.";
                    progress(h, message);
                    printDebug(message);
                    break;
                }

                progress(h, String.format("Negotiating (Gen. %s, Pop. %s, Duration %s).", generation, p.getSize(), (durMS / 1000)), generation);
            }

            progress(h, "Writing outputs.", generation);

            solution = p.getFinalGraphSolution(Selector.SOCIAL);

            solution.commit(t);

            lastConvergeMS = lastChangedMS;
            lastConvergenGen = lastChangedGeneration;

            System.out.println(solution);

            solutionPop = p;
            //generateData(0,p);

            Runnable notify = () -> {
                List<SearchObserver> l = new CopyOnWriteArrayList(context(problem).all(SearchObserver.class));

                for (SearchObserver o : l) {
                    long start = System.currentTimeMillis();
                    o.onSearchCompleted(solution);
                    long dur = System.currentTimeMillis() - start;
                    if (dur > 100) {
                        System.out.format("SLOW EVENT NOTIFICATION: %d ms spend notifying %s.\n", dur, o);
                    }
                }
            };

            if (useSwingThreadNotification) {
                SwingUtilities.invokeAndWait(notify);
            } else {
                notify.run();
            }

            finish(h);

        } catch (InterruptedException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
        } catch (Throwable ex) {
            Exceptions.printStackTrace(ex);
        }

        return new SearchStatistics(generation, durMS);
    }

    private String getFitnessForConcerns(Option solution) {
        StringBuilder strB = new StringBuilder();
        strB.append("Cost values [");
        for (Concern c : solution.getConcerns()) {
            strB.append("(");
            strB.append(c.getName());
            strB.append(" ");
            strB.append(solution.getFitnessValuesMap().get(c));
            strB.append(")");

        }
        strB.append("]");
        return strB.toString();
    }

    /**
     *
     * @return
     */
    @Override
    public Solution getSolution() {

        return solution;
    }

    /**
     *
     * @param configuration
     */
    @Override
    public void setSearchConfiguration(SearchConfiguration configuration) {
        searchCfg.dispose();
        searchCfg = context(problem).add(SearchConfiguration.class, configuration);
    }

    //
    // Private methods
    //
    private void printDebug(Object line) {
        if (getSearchCfg().isDebuggingEnabled()) {
            System.out.println(line);
        }

        // TODO: Export population statistics to CVS file
    }

    private void start(ProgressHandle h, int size) {
        if (h != null) {
            h.start(size);
        }
    }

    private void finish(ProgressHandle h) {
        if (h != null) {
            h.finish();
        }
    }

    private void progress(ProgressHandle h, String message) {
        if (h != null) {
            h.progress(message);
        }
    }

    private void progress(ProgressHandle h, String msg, int progress) {
        if (h != null) {
            h.progress(msg, progress);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public SearchConfiguration getSearchCfg() {
        return context(problem).one(SearchConfiguration.class);
    }

    /**
     *
     * @return
     */
    @Override
    public IPopulation getCurrentPopulation() {
        return solutionPop;
    }

    private String printEntity(Collection<? extends Object> entities) {
        String res = "";
        List<String> objs = new ArrayList<>();
        for (Object obj : entities) {
            objs.add(obj.toString() + "\n");
        }
        Collections.sort(objs);
        for (String str : objs) {
            res += str;
        }
        return res;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Solution> getSolutionList() {
        List<Solution> s = new ArrayList<>();
        s.addAll(solutionPop.getPopulation());
        return s;
    }

    /**
     *
     * @return
     */
    @Override
    public int getGeneration() {
        return generation;
    }

    private String calculateFitnessImprovement(ISolution s) {
        double res = 0.0;
        for (double d : s.getEvaluationValues()) {
            res += d;
        }
        return res + "";
    }

    /**
     *
     * @return
     */
    @Override
    public long getConvergeTime() {
        return lastConvergeMS;
    }

    private void printFront(IPopulation p) {
        //System.out.println("================Begin front================");
        List<ISolution> front = new ArrayList<>(p.getPopulation());
        //sel.select(front);
        StringBuilder str = new StringBuilder();
        for (ISolution s : front) {
            str.append(s.getEvaluationValues());
            str.append("\n");
        }
        System.out.println(str.toString().trim());
        //System.out.println("================End front================");
    }

    /**
     *
     */
    @Override
    public void dispose() {
        searchCfg.dispose();
    }

    /**
     *
     * @return
     */
    @Override
    public int getConvergenGen() {

        return lastConvergenGen;
    }

    /**
     *
     */
    @Override
    public void doEvolution() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isTerminated() {
        return terminated.get();
    }

    @Override
    public void terminate() {
        this.terminated.set(true);
    }

    private void generateData(int gen, IPopulation p) {

        System.out.println("generation = " + gen);
        URL url = getClass().getResource("/gen" + gen + ".csv");

        char commoSep = ',';
        char tabSep = ' ';

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i <= p.getConcerns().size() - 1; i++) {
            sb.append(p.getConcerns().get(i).getName());
            sb.append(tabSep);
        }
        sb.append('\n');

        for (int i = 0; i <= p.getPopulation().size() - 1; i++) {

            Map<Concern, Double> fitnessValuesMap = p.getPopulation().get(i).getFitnessValuesMap();
            for (Map.Entry<Concern, Double> c : fitnessValuesMap.entrySet()) {

                sb.append(c.getValue());
                sb.append(tabSep);
            }
            sb.append('\n');
        }

        try (PrintWriter pw = new PrintWriter(new File(url.toURI()))) {
            pw.write(sb.toString());
            System.out.println("done!");
        } catch (FileNotFoundException | URISyntaxException ex) {
            Exceptions.printStackTrace(ex);
        }

    }
}
