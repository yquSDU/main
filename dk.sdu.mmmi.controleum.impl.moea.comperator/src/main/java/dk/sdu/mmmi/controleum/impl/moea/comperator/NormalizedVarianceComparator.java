/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.comperator;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.Comparator;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class NormalizedVarianceComparator implements Comparator<ISolution> {

    final Map<Concern,MinMax> minMaxConcernFitness;

    public NormalizedVarianceComparator(Map<Concern,MinMax> minMaxConcernFitness) {
        this.minMaxConcernFitness = minMaxConcernFitness;
    }

    @Override
    public int compare(ISolution o1, ISolution o2) {
        int r = 0;
        double f1 = NormalizationUtil.TotalDistanceFromAverage(o1, minMaxConcernFitness);
        double f2 = NormalizationUtil.TotalDistanceFromAverage(o2, minMaxConcernFitness);
        if (f1 != f2) {
            r = (f1 > f2) ? 1 : -1;
        } else {
            r = 0;
        }
        return r;
    }

}
