package dk.sdu.mmmi.controleum.impl.moea.comperator;

import dk.sdu.mmmi.controleum.api.moea.Solution;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author ancla
 */
public class ParetoFitnessComparator implements Comparator<Solution> {

    /**
     * Compares the two solutions using a dominance relation, returning
     * {@code -1} if {@code solution1} dominates {@code solution2}, {@code 1} if
     * {@code solution2} dominates {@code solution1}, and {@code 0} if the
     * solutions are non-dominated.
     *
     * @param solution1 the first solution
     * @param solution2 the second solution
     * @return {@code -1} if {@code solution1} dominates {@code solution2},
     * {@code 1} if {@code solution2} dominates {@code solution1}, and {@code 0}
     * if the solutions are non-dominated
     */
    @Override
    public int compare(Solution solution1, Solution solution2) {

        List<Double> f1 = solution1.getEvaluationValues();
        List<Double> f2 = solution2.getEvaluationValues();

        if (f1.size() != f2.size()) {
            throw new IllegalStateException("The compared solutions have to have equal numbers of objectives.");
        }

        boolean dominate1 = false;
        boolean dominate2 = false;
        
        for (int i = 0; i < f1.size(); i++) {
            if (f1.get(i) < f2.get(i)) {
                dominate1 = true;

                if (dominate2) {
                    return 0;
                }
            } else if (f1.get(i) > f2.get(i)) {
                dominate2 = true;

                if (dominate1) {
                    return 0;
                }
            }
        }

        if (dominate1 == dominate2) {
            return checkEquals(solution1, solution2);
        } else if (dominate1) {
            return -1;
        } else {
            return 1;
        }
    }

    private int checkEquals(Solution solution1, Solution solution2) {
        if (solution1.equals(solution2)) {
            return 1;
        } else {
            return 0;
        }
    }
}
