/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.comperator;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import java.util.Comparator;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class PrioritizedNormalizedFitnessComparator implements Comparator<Solution> {

    Map<Concern, MinMax> minMaxFitnessValues;
    
    public PrioritizedNormalizedFitnessComparator(Map<Concern, MinMax> minMaxFitnessValues)
    {
        this.minMaxFitnessValues = minMaxFitnessValues;
    }
    
    
    @Override
    public int compare(Solution s1, Solution s2) {
        int r = 0;

        for (int p = Concern.HARD_PRIORITY; p <= Concern.LOW_PRIORITY; p++) {

            double f1 = ((ISolution)s1).getSummarizedEvaluationResult(p,minMaxFitnessValues);
            double f2 = ((ISolution)s2).getSummarizedEvaluationResult(p,minMaxFitnessValues);
            if (f1 != f2) {
                r = (f1 > f2) ? 1 : -1;
                break;
            } else {
                r = 0;
            }
        }
        return r;
    }


}
