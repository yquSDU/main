package dk.sdu.mmmi.controleum.api.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;

import java.util.List;

/**
 *
 * @author ancla
 */
public interface GraphSolutionSelector {
    ISolution getSolution(List<ISolution> pop, Selector selector);
    
    IGraph getGraph();
}

