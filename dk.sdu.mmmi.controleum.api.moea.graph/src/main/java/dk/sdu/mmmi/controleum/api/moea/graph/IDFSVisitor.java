package dk.sdu.mmmi.controleum.api.moea.graph;

/**
 * A spanning tree visitor callback interface
 *
 * @see Graph#dfsSpanningTree(Vertex, IDFSVisitor)
 *
 * @author Scott.Stark@jboss.org
 * @version $Revision$
 * @param <T>
 */
public interface IDFSVisitor<T> {
    /**
     * Called by the graph traversal methods when a vertex is first visited.
     *
     * @param g -
     *          the graph
     * @param v -
     *          the vertex being visited.
     */
    public void visit(IGraph<T> g, IVertex<T> v);

    /**
     * Used dfsSpanningTree to notify the visitor of each outgoing edge to an
     * unvisited vertex.
     *
     * @param g -
     *          the graph
     * @param v -
     *          the vertex being visited
     * @param e -
     *          the outgoing edge from v
     */
    public void visit(IGraph<T> g, IVertex<T> v, IEdge<T> e);
}