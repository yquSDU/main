package dk.sdu.mmmi.controleum.api.moea.graph;

public class VisitorException extends Exception {
    public VisitorException(String message) {
        super(message);
    }
}
