package dk.sdu.mmmi.controleum.api.moea.graph;

public interface IEdge<T> {
    /**
     * Get the ending vertex
     *
     * @return ending vertex
     */
    IVertex<T> getTo();

    /**
     * Get the starting vertex
     *
     * @return starting vertex
     */
    IVertex<T> getFrom();

    /**
     * Get the cost of the edge
     *
     * @return cost of the edge
     */
    int getCost();

    /**
     * Set the mark flag of the edge
     *
     */
    void mark();

    /**
     * Clear the edge mark flag
     *
     */
    void clearMark();

    /**
     * Get the edge mark flag
     *
     * @return edge mark flag
     */
    boolean isMarked();

    /**
     * String rep of edge
     *
     * @return string rep with from/to vertex names and cost
     */
    String toString();
}
