package dk.sdu.mmmi.controleum.api.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;

import java.util.List;
import java.util.Map;
import org.w3c.dom.Document;


/**
 * A graph visitor interface.
 * 
 * @author Scott.Stark@jboss.org
 * @version $Revision$
 */
public interface IVisitor<T> {
  /**
   * Called by the graph traversal methods when a vertex is first visited.
   * 
   * @param g - the graph
   * @param v - the vertex being visited.
   */
  List<ISolution> visit(IVertex<T> v, Map<Concern, MinMax> minMaxFitnessValues, Selector rSelector);
  
  Document visit(IVertex<T> v, Document doc) throws VisitorException;
  

}
