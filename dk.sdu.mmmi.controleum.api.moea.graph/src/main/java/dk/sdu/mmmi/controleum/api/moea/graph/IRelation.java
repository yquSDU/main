package dk.sdu.mmmi.controleum.api.moea.graph;

public interface IRelation {
    int getDirection();

    RelativeImportanceConcern getTowards();
}
