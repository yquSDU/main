/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea.graph;

import java.util.List;
import java.util.UUID;

/**
 *
 * @author Anders
 */
public interface RelativeImportanceConcern {
    List<IRelation> getRelation();
    
    Importance getPreferenceImportance();
    
    Integer getDirection(RelativeImportanceConcern c);
    
    void setRelation(List<IRelation> r);
    
    public void setRelationTowards(RelativeImportanceConcern c, RelativeImportance ri);

    public List<UUID> getIssueIds();

    public String getName();

    enum Importance {
     CONSTRAINED, UNCONSTRAINED;
    }
    
    enum RelativeImportance {
        LESS(1,-1),
        EQUAL(0,0),
        MORE(-1,1);
        
        public final int inboundValue;
        public final int outboundValue;
        
        private RelativeImportance(int inboundValue, int outboundValue)
        {
            this.inboundValue = inboundValue;
            this.outboundValue = outboundValue;
        }
    }
}
