package dk.sdu.mmmi.controleum.common.config;

import dk.sdu.mmmi.controleum.common.units.Duration;
import java.io.Serializable;

/**
 * @author mrj
 */
public final class ControlConfig implements Serializable {

	private final long delayMS; // 5 minutes.
	private final boolean controlling; // stopped.
	private final boolean simulating; // stopped.
	private final boolean autoRunning;
	private final boolean simCsvExport; // Simulation data exported to CSV file.

	public static class Builder {

		// Optional parameters - initialized to default values
		private boolean controlling = false;
		private boolean simulating = false;
		private boolean autoRunning = false;
		private long delayMS = 5L * 60 * 1000;
		private boolean simCsvExport = false;

		public Builder() {
		}

		public Builder(ControlConfig cfg) {
			delayMS = cfg.delayMS;
			controlling = cfg.controlling;
			simulating = cfg.simulating;
			autoRunning = cfg.autoRunning;
			simCsvExport = cfg.simCsvExport;
		}

		public Builder delay(long delay) {
			this.delayMS = delay;
			return this;
		}

		public Builder controlling(boolean isControlling) {
			this.controlling = isControlling;
			return this;
		}

		public Builder simulating(boolean isSimulating) {
			this.simulating = isSimulating;
			return this;
		}

		public Builder autoRunning(boolean isAutoRunning) {
			this.autoRunning = isAutoRunning;
			return this;
		}

		public Builder simCsvExport(boolean isSimCsvExport) {
			this.simCsvExport = isSimCsvExport;
			return this;
		}

		public ControlConfig build() {
			return new ControlConfig(this);
		}
	}

	private ControlConfig(Builder b) {
		delayMS = b.delayMS;
		controlling = b.controlling;
		simulating = b.simulating;
		autoRunning = b.autoRunning;
		simCsvExport = b.simCsvExport;
	}

	public Duration getDelay() {
		return new Duration(delayMS);
	}

	public boolean isControlling() {
		return controlling;
	}

	public boolean isSimulating() {
		return simulating;
	}

	public boolean isSimCsvExport() {
		return this.simCsvExport;
	}

	public boolean isAutoRunning() {
		return this.autoRunning;
	}
}
