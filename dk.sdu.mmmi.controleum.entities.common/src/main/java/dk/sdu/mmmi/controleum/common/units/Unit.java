package dk.sdu.mmmi.controleum.common.units;

import java.math.BigDecimal;

/**
 *
 * @author jcs
 */
public interface Unit {

    double value();

    double roundedValue();

    BigDecimal bigDecimal();
}
