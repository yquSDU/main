package dk.sdu.mmmi.controleum.common.units;

import java.math.BigDecimal;

/**
 * @author mrj
 */
public final class Duration implements Unit {

    private final long ms;

    public Duration(Long ms) {      //qqq long -> Long
        this.ms = ms;
    }

    public long toMS() {
        return ms;
    }

    public double toSeconds() {
        return toMS() / 1000d;
    }

    public double toMinutes() {
        return toSeconds() / 60;
    }

    @Override
    public String toString() {
        return String.format("%.1f [HOUR]", toHours());
    }

    public double toHours() {
        return toMinutes() / 60;
    }

    @Override
    public double value() {
        return BigDecimal.valueOf(ms).doubleValue();
    }

    @Override
    public double roundedValue() {
        return value();
    }

    @Override
    public BigDecimal bigDecimal() {
        return BigDecimal.valueOf(ms);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (this.ms ^ (this.ms >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Duration other = (Duration) obj;
        if (this.ms != other.ms) {
            return false;
        }
        return true;
    }

    public Duration add(Duration value) {
        return new Duration(this.toMS() + value.toMS());
    }

    //qqq copied
    public Duration subtract(Duration value) {
        return new Duration(this.toMS() - value.toMS());
    }

    public Duration multiply(int value) {
        return new Duration(this.toMS() * value);
    }
}
