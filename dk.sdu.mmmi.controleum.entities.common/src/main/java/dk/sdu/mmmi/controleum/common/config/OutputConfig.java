package dk.sdu.mmmi.controleum.common.config;



/**
 * @author mrj
 */
public class OutputConfig {

    private boolean enabled = true;

    public OutputConfig() {
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
