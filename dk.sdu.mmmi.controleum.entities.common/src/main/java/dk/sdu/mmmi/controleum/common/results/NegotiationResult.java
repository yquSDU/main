package dk.sdu.mmmi.controleum.common.results;

import java.math.BigDecimal;

/**
 *
 * @author mrj
 */
public class NegotiationResult {

    private int generationsExecuted;
    private long timeSpendMS;    
    private boolean solutionFound;

    public NegotiationResult(int generationsExecuted, long timeSpendMS) {
        this.generationsExecuted = generationsExecuted;
        this.timeSpendMS = timeSpendMS;        
    }

    public Integer getGenerationsExecuted() {
        return generationsExecuted;
    }

    public long getTimeSpendMS() {
        return timeSpendMS;
    }

    public double getTimeSpendSeconds() {
        return BigDecimal.valueOf(timeSpendMS / 1000).doubleValue();
    }

    public boolean isSolutionFound() {
        return solutionFound;
    }
}
