package dk.sdu.mmmi.controleum.common.units;

import java.util.Date;
import java.util.Objects;

/**
 * @author mrj
 */
public class Sample<T> {

    private Date timestamp;
    private T value;

    /**
     * Contructor for Null Object
     */
    public Sample(T value) {
        this.timestamp = new Date(0); // Epoc time
        this.value = value;
    }

    /**
     * @param timestamp The sampling time.
     * @param value The value, or null if sampling failed.
     */
    public Sample(Date timestamp, T value) {
        if (timestamp == null) {
            throw new IllegalArgumentException("Timestamp must not be null!");
        }
        this.timestamp = timestamp;
        this.value = value;
    }

    /**
     * @return the sampling time.
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @return the value or null if sampling failed.
     */
    public T getSample() {
        return value;
    }

    @Override
    public String toString() {
        return getTimestamp() + " @ " + getSample();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.timestamp);
        hash = 79 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sample<T> other = (Sample<T>) obj;
        if (!Objects.equals(this.timestamp, other.timestamp)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        return true;
    }
}
