package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;

import java.util.Comparator;

/**
 *
 * @author AIU
 */
public class PreferenceImportanceComparator implements Comparator<RelativeImportanceConcern> {

    int result=0;

    @Override
    public int compare(RelativeImportanceConcern o1, RelativeImportanceConcern o2) {

        
        if (o1.getPreferenceImportance().compareTo(o2.getPreferenceImportance()) == 0) {
            result = o1.getDirection(o2).compareTo(o2.getDirection(o1));
        } else 
        {
            result = o1.getPreferenceImportance().compareTo(o2.getPreferenceImportance());
        }
        
        return result;
    }
}
