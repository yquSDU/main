/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.moea.graph.IRelation;
import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;
import dk.sdu.mmmi.controleum.control.commons.AbstractConcern;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Anders
 */
public abstract class AbstractRelativeImportanceConcern extends AbstractConcern implements RelativeImportanceConcern {

    private List<IRelation> relations = new ArrayList<>();
    private Importance importance;
    //TODO IMplement the   List<IRelation> setRelationTowards which returns the mirrored relations between the two. 

    public AbstractRelativeImportanceConcern(String name, ControleumContext controlDomain, int priority) {
        super(name, controlDomain, priority);
        importance = Importance.UNCONSTRAINED;
    }

    public AbstractRelativeImportanceConcern(String name, ControleumContext controlDomain, int priority, Importance imp) {
        super(name, controlDomain, priority);
        importance = imp;
    }

    @Override
    public Importance getPreferenceImportance() {
        return importance;
    }

    public List<IRelation> getRelation() {
        return new ArrayList<>(relations);
    }

    @Override
    public Integer getDirection(RelativeImportanceConcern c) {
        Integer direction = 0;
        List<IRelation> rList = this.getRelation();
        for (IRelation r : rList) {
            if (r.getTowards().equals(c)) {
                direction = r.getDirection();
                break;
            }
        }
        return direction;
    }

    @Override
    public void setRelation(List<IRelation> r) {
        relations = r;
    }

    @Override
    public void setRelationTowards(RelativeImportanceConcern c, RelativeImportance ri) {
        List<IRelation> r = new ArrayList<>();
        r.add(new Relation(c, ri.inboundValue));

        this.setRelation(r);

        List<IRelation> r2 = new ArrayList<>();
        r.add(new Relation(this, ri.outboundValue));

        c.setRelation(r2);
    }
}
