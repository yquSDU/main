package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;

import java.util.Comparator;

/**
 *
 * @author AIU
 */
public class RelativeImportanceComparator implements Comparator<RelativeImportanceConcern>{

    @Override
    public int compare(RelativeImportanceConcern o1, RelativeImportanceConcern o2) {
        Integer i1 = o2.getDirection(o1);
        return o1.getDirection(o2).compareTo(i1);
        
    }
    
}

