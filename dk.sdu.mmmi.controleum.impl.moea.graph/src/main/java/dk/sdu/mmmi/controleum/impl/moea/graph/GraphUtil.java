package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.*;
import dk.sdu.mmmi.controleum.api.moea.graph.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;
import static java.lang.System.getProperty;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.openide.util.Exceptions;

/**
 * @author AIU
 */
public class GraphUtil<T> {

    //Graph graph;
    ArrayList<UUID> constrainedIssues = new ArrayList<UUID>();
    List<RelativeImportanceConcern> subCa = new ArrayList<>();

//    public ArrayList getConstrainedIssues(Object cd) {
//
//        List<RelativeImportanceConcern> cas = new ArrayList<>(context(cd).all(RelativeImportanceConcern.class));
//        cas.stream().filter((c) -> (c.isSubsystemAgent() && c.getPreferenceImportance().equals(RelativeImportanceConcern.Importance.CONSTRAINED))).forEach((RelativeImportanceConcern c) -> {
//            subCa.add(c);
//            constrainedIssues.addAll(c.getIssueIds());
//        });
//
//        constrainedIssues = (ArrayList) constrainedIssues.stream().distinct().collect(Collectors.toList());
//        return constrainedIssues;
//    }
//
//
//    // Issue Dependence graph
//    public Graph buildIssueDependenceGraph(Graph g, Object cd) {
//
//        IVertex v = null;
//        constrainedIssues = getConstrainedIssues(cd);
//
//        for (Output output : context(cd).all(Output.class)) {
//            if (g.findVertexByName(output.getName()) == null) {
//                v = new Vertex(output.getName(), UUID.fromString(output.getID()));
//                g.addVertex(v);
//            } else {
//                v = g.findVertexByName(output.getName());
//            }
//
//            for (RelativeImportanceConcern c : context(cd).all(RelativeImportanceConcern.class)) {
//                List<UUID> common = (List<UUID>) c.getIssueIds().stream().filter(constrainedIssues::contains).collect(toList());
//                if (common.contains(UUID.fromString(output.getID()))) {
//                    common.remove(UUID.fromString(output.getID()));
//                }
//                if (c.getIssueIds().contains(UUID.fromString(output.getID())) && (!(common.isEmpty()))) {
//                    for (Output o : context(cd).all(Output.class)) {
//                        for (UUID uuid : common) {
//                            if (UUID.fromString(o.getID()).equals(uuid)) {
//                                if (g.findVertexByName(o.getName()) == null) {
//                                    Vertex v1 = new Vertex(o.getName(), UUID.fromString(o.getID()));
//                                    g.addVertex(v1);
//                                    g.addEdge(v, v1, 0);
//
//                                } else {
//                                    IVertex v1 = g.findVertexByName(o.getName());
//                                    g.addEdge(v, v1, 0);
//                                }
//                                break; // tocheck
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        /////////////////////////////////////////
//        ArrayList<Edge<T>> cycleEdges = g.findCycles();
//
//        if (cycleEdges.size() > 0) {
//            System.out.println("cycleeeeeeeeeee");
//
//            for (Edge ee : cycleEdges) {
//
//                RelativeImportanceConcern c1 = null;
//                RelativeImportanceConcern c2 = null;
//                for (RelativeImportanceConcern ca : subCa) {
//
//                    if (ca.getIssueIds().containsAll(ee.getFrom().getData())) {
//                        c1 = ca;
//                    } else if (ca.getIssueIds().containsAll(ee.getTo().getData())) {
//                        c2 = ca;
//                    }
//                }
//                if (c1.getDirection(c2) < c2.getDirection(c1)) {
//                    g.removeEdge(ee.getFrom(), ee.getTo());
//                    System.out.println(ee.getFrom().getName() + " to " + ee.getTo().getName() + " removed");
//
//                } else if (c2.getDirection(c1) < c1.getDirection(c2)) {
//                    g.removeEdge(ee.getTo(), ee.getFrom());
//                    System.out.println(ee.getTo().getName() + " to " + ee.getFrom().getName() + " removed");
//                } else {
//                    System.out.println("cycle cannot be removed due to same relative importance!!!!");
//                }
//
//            }
//        }
//        ////////////////////////////////////////
//        return g;
//    }
// old graph building method

    public List parseIssueDependenceGraph(Graph g, List<UUID> uuidList) {
        List<UUID> dependentIssues = new ArrayList<>();

        for (UUID u : uuidList) {
            IVertex v = g.findVertexByData(u);
            List<Edge> edges = v.getOutgoingEdges();
            if (edges != null) {
                for (Edge e : edges) {
                    dependentIssues.add((UUID) e.getTo().getData().get(0));
                }
            }
        }
        dependentIssues = (ArrayList) dependentIssues.stream().distinct().collect(Collectors.toList());
        return dependentIssues;
    }

    public static String parseXML(String comparator) {
        String className = null;
        Document doc = null;
        DocumentBuilder dBuilder = null;

        String dir = getProperty("user.home") + File.separator;
        File fXmlFile = new File(dir + "selector.xml");

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }

        try {
            doc = (Document) dBuilder.parse(fXmlFile);
        } catch (SAXException | IOException ex) {
            ex.printStackTrace();
        }

        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("criteria");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            org.w3c.dom.Node nNode = nList.item(temp);
            if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if ((eElement.getAttribute("type")).equals(comparator)) {
                    className = eElement.getElementsByTagName("class").item(0).getTextContent();
                    break;
                }

            }
        }
        //System.out.println(className);
        return className;
    }

    public <T> ListSolutionSelection loadComparator(String className, Map<ISolution, List<Double>> scoreForSolution) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        ListSolutionSelection comp = null;
        try {
            Class compp = ClassLoader.getSystemClassLoader().loadClass(className);
            Constructor cons = compp.getConstructor(Map.class);
            comp = (ListSolutionSelection) cons.newInstance(scoreForSolution);
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return comp;
    }

    public <T> ListSolutionSelection parseXML_loadComparator(Selector comparator, Map<ISolution, List<Double>> scoreForSolution, Map<Concern, MinMax> minMaxFitnessValues) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        String className = null;
        Document doc = null;
        DocumentBuilder dBuilder = null;
        String dir = getProperty("user.home") + File.separator;
        File fXmlFile = new File(dir + "selector.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }

        try {
            doc = (Document) dBuilder.parse(fXmlFile);
        } catch (SAXException | IOException ex) {
            ex.printStackTrace();
        }

        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("criteria");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            org.w3c.dom.Node nNode = nList.item(temp);
            if (nNode.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                if ((eElement.getAttribute("type")).equals(comparator.toString())) {
                    className = eElement.getElementsByTagName("class").item(0).getTextContent();
                    break;
                }

            }
        }
        ListSolutionSelection comp = null;
        try {
            Class compp = ClassLoader.getSystemClassLoader().loadClass(className);
            Constructor cons = compp.getConstructor(Map.class, Map.class);
            comp = (ListSolutionSelection) cons.newInstance(scoreForSolution, minMaxFitnessValues);
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return comp;
    }

    public void generateGraphXML(IGraph graph) throws ParserConfigurationException, VisitorException {
        DepthFirstVisitor df = new DepthFirstVisitor();
        Document dom = graph.depthFirstSearch(graph.getRootVertex(), df);

        String dir = getProperty("user.home") + File.separator + "RIGraphs" + File.separator;
        String xmlPath = dir + "graph_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")) + ".xml";
        System.out.println("Issue dependency graph located at: " + xmlPath);

        File f1 = new File(dir);
        try {
            if (Files.notExists(Paths.get(dir))) {
                Files.createDirectory(Paths.get(dir));
            }
            File file = new File(xmlPath);
            file.createNewFile();

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source = new DOMSource(dom);

            StreamResult result = new StreamResult(xmlPath);
            transformer.transform(source, result);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (TransformerException ex) {
            Exceptions.printStackTrace(ex);
        }

    }
}
