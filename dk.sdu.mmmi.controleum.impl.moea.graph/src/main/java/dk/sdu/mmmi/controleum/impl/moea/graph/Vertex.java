package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.graph.IEdge;
import dk.sdu.mmmi.controleum.api.moea.graph.IVertex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * A named graph vertex with optional data.
 *
 * @param <T>
 * @author Scott.Stark@jboss.org
 * @version $Revision$
 */
@SuppressWarnings("unchecked")
public class Vertex<T> implements IVertex<T> {
    private List<IEdge<T>> incomingEdges;

    private List<IEdge<T>> outgoingEdges;

    private String name;

    private String selector;

    private boolean mark;

    private int markState;

    private List<T> data = new ArrayList<>();

    /**
     * Calls this(null, null).
     */
    public Vertex() {
        this(null, new ArrayList<>());
    }

    /**
     * Create a vertex with the given name and no data
     *
     * @param n
     */
    public Vertex(String n) {
        this(n, new ArrayList<>());
    }

    /**
     * Create a Vertex with name n and given data
     *
     * @param n    -
     *             name of vertex
     * @param data -
     *             data associated with vertex
     */
    public Vertex(String n, T data) {
        incomingEdges = new ArrayList<>();
        outgoingEdges = new ArrayList<>();
        name = n;
        mark = false;
        this.data.add(data);
        //selector="approxfairness";
        selector = "social";
    }

    public Vertex(String n, List<T> data) {
        incomingEdges = new ArrayList<>();
        outgoingEdges = new ArrayList<>();
        name = n;
        mark = false;
        this.data = data;
        //selector="approxfairness";
        selector = "social";
    }

    public Vertex(String n, List<T> data, String selector) {
        incomingEdges = new ArrayList<>();
        outgoingEdges = new ArrayList<>();
        name = n;
        mark = false;
        this.data = data;
        //selector="approxfairness";
        this.selector = selector;
    }

    /**
     * @return the possibly null name of the vertex
     */
    public String getName() {
        return name;
    }

    public String getSelector() {
        return this.selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    /**
     * @return the possibly null data of the vertex
     */
    public List<T> getData() {
        return this.data;
    }

    public boolean isRoot() {

        boolean result = false;
        if (this.getIncomingEdges().size() < 1) {
            result = true;
        }

        return result;
    }

    /**
     * @param data The data to set.
     */
    public void addData(T data) {
        this.data.add(data);
    }

    /**
     * @param data The data to remove.
     */
    public void removeData(T data) {
        this.data.remove(data);
    }

    /**
     * Add an edge to the vertex. If edge.from is this vertex, its an outgoing
     * edge. If edge.to is this vertex, its an incoming edge. If neither from or
     * to is this vertex, the edge is not added.
     *
     * @param e -
     *          the edge to add
     * @return true if the edge was added, false otherwise
     */
    public boolean addEdge(IEdge<T> e) {
        if (e.getFrom() == this)
            outgoingEdges.add(e);
        else if (e.getTo() == this)
            incomingEdges.add(e);
        else
            return false;
        return true;
    }

    /**
     * Add an outgoing edge ending at to.
     *
     * @param to   -
     *             the destination vertex
     * @param cost the edge cost
     */
    public void addOutgoingEdge(IVertex<T> to, int cost) {
        IEdge<T> out = new Edge<T>(this, to, cost);
        outgoingEdges.add(out);
    }

    /**
     * Add an incoming edge starting at from
     *
     * @param from -
     *             the starting vertex
     * @param cost the edge cost
     */
    public void addIncomingEdge(IVertex<T> from, int cost) {
        IEdge<T> out = new Edge<T>(this, from, cost);
        incomingEdges.add(out);
    }

    /**
     * Check the vertex for either an incoming or outgoing edge mathcing e.
     *
     * @param e the edge to check
     * @return true it has an edge
     */
    public boolean hasEdge(IEdge<T> e) {
        if (e.getFrom() == this)
            return incomingEdges.contains(e);
        else if (e.getTo() == this)
            return outgoingEdges.contains(e);
        else
            return false;
    }

    /**
     * Remove an edge from this vertex
     *
     * @param e -
     *          the edge to remove
     * @return true if the edge was removed, false if the edge was not connected
     * to this vertex
     */
    public boolean remove(IEdge<T> e) {
        if (e.getFrom() == this)
            //incomingEdges.remove(e);
            this.outgoingEdges.remove(e);
        else if (e.getTo() == this)
            //outgoingEdges.remove(e);
            incomingEdges.remove(e);
        else
            return false;
        return true;
    }

    public boolean removes(IEdge<T> e) {
        if (e.getFrom() == this)
            incomingEdges.remove(e);

        else if (e.getTo() == this)
            outgoingEdges.remove(e);

        else
            return false;
        return true;
    }


    public void removeEdges() {
        this.incomingEdges.clear();
        this.outgoingEdges.clear();
    }


    public void removeEdgesExcept(IEdge<T> e) {
        Iterator<IEdge<T>> it = this.incomingEdges.iterator();
        while (it.hasNext()) {
            IEdge<T> ed = it.next();
            if (!(ed.equals(e))) {
                IVertex v = ed.getFrom();
                v.remove(ed);
                it.remove();
                //this.incomingEdges.remove(ed);
            }
        }

    }

    /**
     * @return the count of incoming edges
     */
    public int getIncomingEdgeCount() {
        return incomingEdges.size();
    }

    /**
     * Get the ith incoming edge
     *
     * @param i the index into incoming edges
     * @return ith incoming edge
     */
    public IEdge<T> getIncomingEdge(int i) {
        return incomingEdges.get(i);
    }

    /**
     * Get the incoming edges
     *
     * @return incoming edge list
     */
    public List getIncomingEdges() {
        return this.incomingEdges;
    }

    public int getIncomingEdgesCost() {
        int tCost = 0;
        List<Edge> ed = this.getIncomingEdges();
        for (Edge e : ed) {
            tCost = tCost + e.getCost();
        }
        return tCost;
    }

    /**
     * @return the count of incoming edges
     */
    public int getOutgoingEdgeCount() {
        return outgoingEdges.size();
    }

    /**
     * Get the ith outgoing edge
     *
     * @param i the index into outgoing edges
     * @return ith outgoing edge
     */
    public IEdge<T> getOutgoingEdge(int i) {
        return outgoingEdges.get(i);
    }

    /**
     * Get the outgoing edges
     *
     * @return outgoing edge list
     */
    public List getOutgoingEdges() {
        return this.outgoingEdges;
    }

    /**
     * Search the outgoing edges looking for an edge whose's edge.to == dest.
     *
     * @param dest the destination
     * @return the outgoing edge going to dest if one exists, null otherwise.
     */
    public IEdge<T> findEdgeTo(IVertex<T> dest) {
        for (IEdge<T> e : outgoingEdges) {
            if (e.getTo() == dest)
                return e;
        }
        return null;
    }

    public IEdge<T> findEdgeFrom(IVertex<T> src) {
        for (IEdge<T> e : incomingEdges) {
            if (e.getFrom() == src)
                return e;
        }
        return null;
    }

    public IEdge<T> findEdgeFromRoot(IVertex<T> from) {
        Edge ed = null;
        for (IEdge<T> e : incomingEdges) {
            if (e.getFrom() == from) {
                return e;
            } else {
                if (!(e.getFrom().isRoot())) {
                    return e.getFrom().findEdgeFromRoot(from);
                }
            }
        }
        return ed;
    }

    public int findCostFromRoot(IVertex<T> from) {
        int retCost = -1;
        for (IEdge<T> e : incomingEdges) {
            if (e.getFrom() == from) {
                return retCost += e.getCost();
            } else {
                //retCost+=e.getCost();
                if (!(e.getFrom().isRoot())) {
                    retCost += e.getFrom().findCostFromRoot(from);
                }
            }
        }
        return retCost;
    }

    /**
     * Search the outgoing edges for a match to e.
     *
     * @param e -
     *          the edge to check
     * @return e if its a member of the outgoing edges, null otherwise.
     */
    public IEdge<T> findEdge(IEdge<T> e) {
        if (outgoingEdges.contains(e))
            return e;
        else
            return null;
    }

    /**
     * What is the cost from this vertext to the dest vertex.
     *
     * @param dest -
     *             the destination vertex.
     * @return Return Integer.MAX_VALUE if we have no edge to dest, 0 if dest is
     * this vertex, the cost of the outgoing edge otherwise.
     */
    public int cost(IVertex<T> dest) {
        if (dest == this)
            return 0;

        IEdge<T> e = findEdgeTo(dest);
        int cost = Integer.MAX_VALUE;
        if (e != null)
            cost = e.getCost();
        return cost;
    }

    /**
     * Is there an outgoing edge ending at dest.
     *
     * @param dest -
     *             the vertex to check
     * @return true if there is an outgoing edge ending at vertex, false
     * otherwise.
     */
    public boolean hasEdge(IVertex<T> dest) {
        return (findEdgeTo(dest) != null);
    }

    /**
     * Has this vertex been marked during a visit
     *
     * @return true is visit has been called
     */
    public boolean visited() {
        return mark;
    }

    /**
     * Set the vertex mark flag.
     */
    public void mark() {
        mark = true;
    }

    /**
     * Set the mark state to state.
     *
     * @param state the state
     */
    public void setMarkState(int state) {
        markState = state;
    }

    /**
     * Get the mark state value.
     *
     * @return the mark state
     */
    public int getMarkState() {
        return markState;
    }

    /**
     * Visit the vertex and set the mark flag to true.
     */
    public void visit() {
        mark();
    }


    /**
     * Clear the visited mark flag.
     */
    public void clearMark() {
        mark = false;
    }

    /**
     * @return a string form of the vertex with in and out edges.
     */
    public String toString() {
        StringBuffer tmp = new StringBuffer("Vertex(");
        tmp.append(name);
        tmp.append(", data=");
        tmp.append(data);
        tmp.append("), in:[");
        for (int i = 0; i < incomingEdges.size(); i++) {
            IEdge<T> e = incomingEdges.get(i);
            if (i > 0)
                tmp.append(',');
            tmp.append('{');
            tmp.append(e.getFrom().getName());
            tmp.append(',');
            tmp.append(e.getCost());
            tmp.append('}');
        }
        tmp.append("], out:[");
        for (int i = 0; i < outgoingEdges.size(); i++) {
            IEdge<T> e = outgoingEdges.get(i);
            if (i > 0)
                tmp.append(',');
            tmp.append('{');
            tmp.append(e.getTo().getName());
            tmp.append(',');
            tmp.append(e.getCost());
            tmp.append('}');
        }
        tmp.append(']');
        return tmp.toString();
    }
}
