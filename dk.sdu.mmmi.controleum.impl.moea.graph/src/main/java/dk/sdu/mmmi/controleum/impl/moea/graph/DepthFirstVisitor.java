package dk.sdu.mmmi.controleum.impl.moea.graph;

import com.decouplink.Utilities;
import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.*;
import dk.sdu.mmmi.controleum.api.moea.graph.*;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu.SocialSelector;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;
import org.w3c.dom.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;

/**
 *
 * @author AIU
 */
public class DepthFirstVisitor<T> implements IVisitor<T> {

    List<ISolution> solPopulation = new ArrayList<>();
    Map<ISolution, List<Double>> solutionScores = new HashMap<>();
    Selector rSelector;
    GraphUtil gu = new GraphUtil();
    ListSolutionSelection prioritySelector = null;
    ControleumContext cd = null;        //qqq add

    public DepthFirstVisitor() {
        this.solPopulation = new ArrayList<>();
    }

    public DepthFirstVisitor(List<ISolution> solPopulation) {
        this.solPopulation = solPopulation;

    }

    @Override
    public List<ISolution> visit(IVertex<T> v, Map<Concern, MinMax> minMaxFitnessValues, Selector rSelector) {
        //qqq   root vertex,       min max fitness of 7 concerns,     SOCIAL
        // if different selector is used for each vertex
        //this.rSelector=v.getSelector();

        //if same selector is used for all vertices
        this.rSelector = rSelector;
        List<Double> values = new ArrayList<>();
        solutionScores = new HashMap<>();
        List<T> data = v.getData();                                                     //qqq data: root vertex concerns
        Map<Concern, Double> normalizedSolution;
        cd = (ControleumContext) solPopulation.get(0).getContext();                     //qqq add, for getting all concerns

        for (ISolution s : solPopulation) {
            normalizedSolution = NormalizationUtil.NormalizeSolution(s, minMaxFitnessValues);
            for (Concern CC : Utilities.context(cd).all(Concern.class)) {               //qqq add
                for (T c : data) { // T typically ConcernAgent
                    if (((Concern) c).getName().equals(CC.getName())) {                 //qqq add, for avoiding normalizedSolution.get(c) == null
//                      Double normalizedS = normalizedSolution.get(((Concern) c));     //qqq for debug //qqq !!! normalizedSolution.get(c) == null !!!
                        values.add(normalizedSolution.get(CC));                         //qqq using the concern from "context" rather than from "data"
                    }
                }
            }
            solutionScores.put(s, values);
            values = new ArrayList<>();
        }

        try {
            // parsing XML file and load Comparator //qqq "C:\Users\yqu\selector.xml"

            //qqq prioritySelector = gu.parseXML_loadComparator(rSelector, solutionScores, minMaxFitnessValues);    //TODO //qqq comment
            prioritySelector = new SocialSelector(solutionScores, minMaxFitnessValues); //TODO //qqq add
        } catch (/*InstantiationException | IllegalAccessException |*/IllegalArgumentException /*| InvocationTargetException*/ ex) {
            ex.printStackTrace();
        }

        // selecting list of candidate solutions for each priority group
        List<ISolution> population = prioritySelector.select(solPopulation);
        return population;

    }

    @Override
    public Document visit(IVertex v, Document doc) throws VisitorException {
        List<RelativeImportanceConcern> data = v.getData();

        Element rootElement = doc.getDocumentElement();

        // node elements
        Element agent = doc.createElement("node");
        rootElement.appendChild(agent);

        // set attribute to node element
        Attr attr = doc.createAttribute("name");
        attr.setValue(v.getName());
        agent.setAttributeNode(attr);
        Element concern = null;
        Element issue = null;
        for (RelativeImportanceConcern ca : data) {
            List<UUID> issues = ca.getIssueIds();
            for (UUID o : issues) {
                issue = checkIssueExists(doc, o, agent);
                if (issue == null) {
                    issue = doc.createElement("issue");
                    attr = doc.createAttribute("id");
                    attr.setValue(o.toString());
                    issue.setAttributeNode(attr);
                }

                concern = doc.createElement("concern");
                attr = doc.createAttribute("name");
                attr.setValue(ca.getName());
                concern.setAttributeNode(attr);

                attr = doc.createAttribute("preference_importance");
                //if (ca.getInput() != null) 
                {
                    attr.setValue(ca.getPreferenceImportance().toString());
                }
                //else {
                // attr.setValue("unconstrained");
                //}
                concern.setAttributeNode(attr);
                issue.appendChild(concern);
                agent.appendChild(issue);
            }
        }
        return doc;
    }

    public Element checkIssueExists(Document doc, UUID u, Element parent) {

        Element e = null;
        NodeList nList = doc.getElementsByTagName("issue");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                if ((eElement.getAttribute("id")).equals(u.toString()) && eElement.getParentNode().isEqualNode(parent)) {
                    e = eElement;
                    break;

                }

            }
        }
        return e;
    }
}
