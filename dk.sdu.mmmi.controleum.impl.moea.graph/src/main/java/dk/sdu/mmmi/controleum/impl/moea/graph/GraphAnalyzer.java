package dk.sdu.mmmi.controleum.impl.moea.graph;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.getProperty;

/**
 *
 * @author AIU
 */
public class GraphAnalyzer<T> {
    private Graph graph;
    private boolean isFine=true;
    public GraphAnalyzer(Graph graph)
    {
        this.graph=graph;
    }
    
    public void generateAnalysis()
    {
         //Detect cycles
        if (graph.findCycles().size() > 0) {
            System.out.println("cycles");
           
        } else {
            System.out.println("no cycles");
        }
        identifyConflict();
        
    }
    
    
    public void identifyConflict() 
    {
      String className = null;
        Document doc = null;
        DocumentBuilder dBuilder = null;
        String dir = getProperty("user.home") + File.separator+"Files"+File.separator;
        File fXmlFile = new File(dir+"graph_" + graph.getName() + ".xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        }

        try {
            doc = (Document) dBuilder.parse(fXmlFile);
        } catch (SAXException | IOException ex) {
            ex.printStackTrace();
        }

        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("issue");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                NodeList children = nNode.getChildNodes();
                Node parent=nNode.getParentNode();
                List<Element> constrained=new ArrayList<>();
                for (int tempp = 0; tempp < children.getLength(); tempp++) {
                    Node nNodee = children.item(tempp);
                    if (nNodee.getNodeType() == Node.ELEMENT_NODE) {
                        Element eeElement = (Element) nNodee;
                        if ((eeElement.getAttribute("goal_importance")).equals("CONSTRAINED") && eeElement.getParentNode().getParentNode().isEqualNode(parent)) {
                            constrained.add(eeElement);
                        }
                    }
                

                }
                
                if(constrained.size()>1)
                {
                    System.out.print("Potential Conflict in "+graph.getName()+": ");
                    for(Element n:constrained)
                    {
                        
                        System.out.print(n.getAttribute("name")+", ");
                    }
                    System.out.println();
                }
                
            }
       
        }
    }
}
