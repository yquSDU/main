package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.*;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphSolutionSelector;
import dk.sdu.mmmi.controleum.api.moea.graph.GraphType;
import dk.sdu.mmmi.controleum.api.moea.graph.Selector;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.decouplink.Utilities.context;

import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;


/**
 *
 * @author AIU
 */
public class GraphBasedFinalSolutionSelection implements GraphSolutionSelector {

    
    List<ISolution> pop = new ArrayList<>();
    Graph g;
    ControleumContext cd = null;
    GraphUtil gUtil = new GraphUtil();
    Map<Concern, MinMax> minMaxFitnessValues;
    

    @Override
    public ISolution getSolution(List<ISolution> solPopulation, Selector rSelector) {

        this.pop = solPopulation;
        cd = (ControleumContext) solPopulation.get(0).getContext();
        for(Graph gIndex:context(cd).all(Graph.class))
        {
            if(gIndex.getGraphType().equals(GraphType.CONCERN))
            g = gIndex;
        }

        Collection<? extends RelativeImportanceConcern> concernCollection = context(cd).all(RelativeImportanceConcern.class);
        List<RelativeImportanceConcern> concerns = new ArrayList<>(concernCollection);

        minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(solPopulation);
        g=new Graph(g.getName(),concerns,new PreferenceImportanceComparator());
        
        g.buildGraph();
        System.out.println(cd.getName() + ": Post Negotiation " + g.toString());
        GraphAnalyzer analyser=new GraphAnalyzer(g);
         
        //Uncomment if analysis required
          /*  try {
                result = analyser.generateAnalysis();
            } catch (ParserConfigurationException ex) {
                Exceptions.printStackTrace(ex);
            }
                if(!result)
                {
                    System.out.println("Analysis failed");
                }*/
                //System.out.println(cd.getName()+": Post Negotiation "+g.toString());
        
        DepthFirstVisitor df=new DepthFirstVisitor(pop);
        pop=g.depthFirstSearch(g.getRootVertex(),df,minMaxFitnessValues,rSelector);
        return pop.get(0);
    }

     @Override
    public Graph getGraph()
    {
        return g;
    }

}
