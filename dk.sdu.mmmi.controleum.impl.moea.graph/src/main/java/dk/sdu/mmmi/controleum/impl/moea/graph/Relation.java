/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.graph;

import dk.sdu.mmmi.controleum.api.moea.graph.RelativeImportanceConcern;
import dk.sdu.mmmi.controleum.api.moea.graph.IRelation;

/**
 *
 * @author aiu
 */
public class Relation implements IRelation{
    
    private RelativeImportanceConcern towards;
    private int direction;
    
    public Relation(RelativeImportanceConcern towards, int direction){
        this.towards=towards;
        this.direction=direction;
    }
    
    
    @Override
    public int getDirection()
    {
        return this.direction;
    }
    
    @Override
    public RelativeImportanceConcern getTowards(){
        return this.towards;
    }
    
}
