/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.clusteredparetoselection;

/**
 *
 * @author ancla
 */
public class DistancePair implements Comparable {

    private final double distance;
    private final Cluster c1;
    private final Cluster c2;

    public DistancePair(double distance, Cluster c1, Cluster c2) {
        this.distance = distance;
        this.c1 = c1;
        this.c2 = c2;
    }

    @Override
    public int compareTo(Object o) {
        DistancePair dp = (DistancePair) o;
        return Double.compare(this.distance, dp.distance);
    }

    public Cluster getC1() {
        return c1;
    }

    public Cluster getC2() {
        return c2;
    }
    
    public boolean contains(Cluster c)
    {
        return c1.equals(c) || c2.equals(c);    
    }
}
