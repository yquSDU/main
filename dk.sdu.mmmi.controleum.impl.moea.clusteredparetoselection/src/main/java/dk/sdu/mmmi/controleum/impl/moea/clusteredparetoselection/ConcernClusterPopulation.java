package dk.sdu.mmmi.controleum.impl.moea.clusteredparetoselection;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.impl.moea.comperator.ParetoFitnessComparator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ancla
 */
public class ConcernClusterPopulation {

    private List<ScoreSolution> scoreSolutions;
    private ParetoFitnessComparator PARETO_FITNESS = new ParetoFitnessComparator();

    public ConcernClusterPopulation(List<Concern> concerns) {
        scoreSolutions = new ArrayList<>();
        for (Concern c : concerns) {
            scoreSolutions.add(new ScoreSolution(c));
        }
    }

    public void testSolution(ISolution s) {
        for (ScoreSolution ss : scoreSolutions) {
            ss.testSolution(s);
        }
    }

    public List<ISolution> getPop() {
        List<ISolution> result = new ArrayList<>();
        for (ScoreSolution s : scoreSolutions) {
            if (!result.contains(s.solution)) {
                result.add(s.solution);
            }
        }

        return result;
    }

    private class ScoreSolution {

        private Concern c;
        private double score = Double.MAX_VALUE;
        private ISolution solution;

        public ScoreSolution(Concern c) {
            this.c = c;
        }

        public void testSolution(ISolution s) {
            if(c.evaluate(s) < score)
            {
                solution = s;
                score = c.evaluate(s);
            }
            else if (c.evaluate(s) == score && PARETO_FITNESS.compare(s, solution) == -1) {
                solution = s;
            }
        }

        public ISolution getSolution() {
            return solution;
        }

    }

}
