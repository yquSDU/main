/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.clusteredparetoselection;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.IPopulationSelector;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.impl.moea.comperator.ParetoFitnessComparator;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.ApproximatedFairnessSelectorI;
import dk.sdu.mmmi.controleum.impl.moea.solutionselectors.MaximizeSocialWelfareSelector;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author ancla
 */
public class ClusteredParetoOptimalSelector implements IPopulationSelector {

    public static final double EPS = 1e-10;
    private static final ParetoFitnessComparator PARETO_FITNESS = new ParetoFitnessComparator();
    private int maxPop, reduceTo;
    private ConcernClusterPopulation concernClusterPopulation;


    public ClusteredParetoOptimalSelector(int maxPop, int reduceTo) {
        this.maxPop = maxPop;
        this.reduceTo = reduceTo;
    }


    private List<ISolution> getClusteredPopulation(List<ISolution> pop) {
        ClusterSelection clusterSelection = new ClusterSelection(pop);
        clusterSelection.reduceParetoSet(reduceTo);
        List<ISolution> clusteredPop = clusterSelection.getReducedFront();
        clusteredPop.addAll(getBestSolutions(pop));
        return clusteredPop;
    }


    private Collection<? extends ISolution> getBestSolutions(List<ISolution> pop) {
        ISolution firstSol = pop.get(0);
        List<ISolution> bestSolutions = new ArrayList<>();
        ISolutionSelector maxSocial = new MaximizeSocialWelfareSelector();
        ISolution maxProfit = maxSocial.getSolution(pop);
        bestSolutions.add(maxSocial.getSolution(pop));
        ISolutionSelector maxFair = new ApproximatedFairnessSelectorI();
        ISolution mostFair = maxFair.getSolution(pop);
        if (!bestSolutions.contains(mostFair)) {
            bestSolutions.add(mostFair);
        }
        for (Concern c : firstSol.getConcerns()) {
            double bestScore = firstSol.getEvaluationValue(c.getClass());
            ISolution bestSol = firstSol;
            for (ISolution s : pop) {
                double score = s.getEvaluationValue(c.getClass());
                if (score < bestScore) {
                    bestScore = score;
                    bestSol = s;
                }
            }
            if (!bestSolutions.contains(bestSol)) {
                bestSolutions.add(bestSol);
            }
        }
        return bestSolutions;
    }

    @Override
    public boolean selectParetoFront(List<ISolution> paretoFront, List<ISolution> newPopulation) {
        if (concernClusterPopulation == null) {
            concernClusterPopulation = new ConcernClusterPopulation(newPopulation.get(0).getConcerns());
        }
        boolean changed = false;

        for (ISolution s : newPopulation) {
            Map<Integer, List<ISolution>> sortByDomination = paretoFront.stream().collect(Collectors.groupingBy(sR -> PARETO_FITNESS.compare(s, sR)));
            List<ISolution> newSolutionDominatedBy = sortByDomination.get(1);
            if (newSolutionDominatedBy == null) {
                paretoFront.add(s);
                concernClusterPopulation.testSolution(s);
                changed = true;
                List<ISolution> solutionsDominatedByNewSolution = sortByDomination.get(-1);
                if (solutionsDominatedByNewSolution != null) {
                    paretoFront.removeAll(solutionsDominatedByNewSolution);
                }

                if (paretoFront.size() > maxPop) {
                    List<ISolution> clusteredPop = getClusteredPopulation(paretoFront);

                    paretoFront = clusteredPop;

                    //TODO This is an important metric! Tells us complexity -> Clusteredpop.size() - Number of concerns -> Complexity value
                    System.out.println(paretoFront.size());
                }
                changed = true;
            }
        }

        return changed;
    }
}
