/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.clusteredparetoselection;

import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ancla
 */
public class Cluster {

    private final List<ISolution> solutions = new ArrayList<>();
    private List<Double> virtualCentroid = new ArrayList<>();
    private ISolution physicalCentroid = null;

    public Cluster(ISolution s) {
        solutions.add(s);
        virtualCentroid = s.getEvaluationValues();
        physicalCentroid = s;
    }

    public void merge(Cluster c) {
        this.solutions.addAll(c.solutions);
        virtualCentroid = calculateVirtualCentroid();
    }

    public void addSolution(ISolution s) {
        solutions.add(s);
        virtualCentroid = calculateVirtualCentroid();
    }

    public boolean removeSolution(ISolution s) {
        if (solutions.remove(s)) {
            if (!solutions.isEmpty()) {
                virtualCentroid = calculateVirtualCentroid();
            }
            return true;
        }
        return false;
    }

    public ISolution calculatePhysicalCentroid() {
        double minDistance = Double.MAX_VALUE;
        ISolution representativeSolution = null;
        for (ISolution s : solutions) {
            double distance = findDistanceToVirtualCentroid(s.getEvaluationValues());
            if (distance < minDistance) {
                representativeSolution = s;
                minDistance = distance;
            }
        }
        return representativeSolution;
    }

    public List<Double> calculateVirtualCentroid() {
        List<Double> sumFitnessForConcerns = null;
        for (ISolution s : solutions) {
            if (sumFitnessForConcerns == null) {
                sumFitnessForConcerns = new ArrayList<>(s.getEvaluationValues());
            } else {
                for (int i = 0; i < sumFitnessForConcerns.size(); i++) {
                    sumFitnessForConcerns.set(i, sumFitnessForConcerns.get(i) + s.getEvaluationValues().get(i));
                }
            }
        }
        int n = solutions.size();
        List<Double> vCentroid = new ArrayList<>();
        for (Double d : sumFitnessForConcerns) {
            vCentroid.add(d / n);
        }
        return vCentroid;
    }

    public double findDistanceToVirtualCentroid(List<Double> s) {
        double distance = 0.0;
        for (int i = 0; i < virtualCentroid.size(); i++) {
            distance += Math.pow(virtualCentroid.get(i) - s.get(i), 2);
        }
        return distance;
    }

    List<Double> getVirtualCentroid() {
        return virtualCentroid;
    }

    ISolution getPhysicalCentroid() {
        return calculatePhysicalCentroid();
    }

    int size() {
        return solutions.size();
    }

}
