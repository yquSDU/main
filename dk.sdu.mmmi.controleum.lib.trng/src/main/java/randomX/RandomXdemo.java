package randomX;

public class RandomXdemo {

    public static void main(String[] args) {
        RandomX r;
        int i;

        r = new RandomHotBits();

        long mean = 0;
        long n = 100000000;
        for (i = 0; i < n; i++) {
            int b = r.nextByte() & 0xFF;
            mean += b;
        }

        System.out.print("Mean for ");
        System.out.print(n);
        System.out.print(" values is ");
        System.out.println((double) mean / n);
    }
}
