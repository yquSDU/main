package dk.sdu.mmmi.controleum.impl.control;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.control.ControlManager;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import dk.sdu.mmmi.controleum.api.simulation.SimulationManager;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import org.openide.util.lookup.ServiceProvider;
import dk.sdu.mmmi.controleum.api.core.ControleumContext;
import java.util.UUID;

/**
 * @author mrj
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    private final ScheduledExecutorService executor;

    public Plugin() {
        executor = Executors.newScheduledThreadPool(5);
    }

    @Override
    public Disposable create(ControleumContext g, UUID issueId) {   //qqq add UUID issueId

        DisposableList d = new DisposableList();

        // Scheduler for background tasks
        d.add(context(g).add(ScheduledExecutorService.class, executor));
        d.add(context(g).add(ControlManager.class, new ControlManagerImpl(g)));
        d.add(context(g).add(SimulationManager.class, new SimulationManagerImpl(g)));

        return d;
    }
}
