/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic;

/**
 *
 * @author ancla
 */
public interface BasicListener<T extends BasicEvent> {

    public void notifyAdded(T e);
    public void notifyModified(T e);
    public void notifyDeleted(T e);
}
