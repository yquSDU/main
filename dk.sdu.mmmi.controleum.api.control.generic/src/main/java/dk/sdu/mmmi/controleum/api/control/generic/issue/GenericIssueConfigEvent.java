/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic.issue;

import dk.sdu.mmmi.controleum.api.control.generic.BasicEvent;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public class GenericIssueConfigEvent extends BasicEvent<GenericIssueConfig> {
    
    public GenericIssueConfigEvent(UUID id, GenericIssueConfig value) {
        super(id, value);
    }
}
