/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public class ValueEvent<T> extends BasicEvent<T> {


    public ValueEvent(UUID id, T value) {
        super(id, value);

    }
}
