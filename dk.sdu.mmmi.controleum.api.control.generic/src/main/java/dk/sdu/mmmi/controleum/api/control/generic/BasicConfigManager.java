/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic;

import java.util.List;
import java.util.UUID;

/**
 *
 * @author ancla
 */
public interface BasicConfigManager<T extends BasicConfig> {

    List<UUID> getEntityIds();

    T getConfig(UUID id);

    void setConfig(UUID id, T cfg);

    public void add(T cfg);

    public void delete(UUID id);

}
