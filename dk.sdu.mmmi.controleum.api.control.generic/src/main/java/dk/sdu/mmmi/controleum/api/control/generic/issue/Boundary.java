package dk.sdu.mmmi.controleum.api.control.generic.issue;

public class Boundary {

    private final Number upperBoundary;
    private final Number lowerBoundary;
    private final Number resolution;

    /**
     * Boundary object used for setting the boundaries for the randomly generated values used in a Issue
     * @param upperBoundary Max random value generated for each slot in a issue
     * @param lowerBoundary Min random value generated for each slot in a issue
     * @param resolution Interval between possible values for the issue slots. eg. if set to 2, the values found in the issue slots are all a factor of 2.
     */
    public Boundary(Number upperBoundary, Number lowerBoundary, Number resolution) {
        this.upperBoundary = upperBoundary;
        this.lowerBoundary = lowerBoundary;
        this.resolution = resolution;
        validateInputs();
    }

    public Number getUpperBoundary() {
        return upperBoundary;
    }

    public Number getLowerBoundary() {
        return lowerBoundary;
    }

    public Number getResolution() {
        return resolution;
    }

    private void validateInputs(){

        if (upperBoundary == null || lowerBoundary == null || resolution == null){
            throw new IllegalArgumentException("Inputs must not be null, for inputs: \n" +
                    "upperBoundary: " + upperBoundary + "\n" +
                    "lowerBoundary: "   + lowerBoundary + "\n" +
                    "resolution: " + resolution + "\n");
        }

        if (upperBoundary.getClass() != lowerBoundary.getClass() || upperBoundary.getClass() != resolution.getClass()) {
            throw new IllegalArgumentException("Inputs not of same type, for inputs: \n" +
                    "upperBoundary: " + upperBoundary.getClass() + "\n" +
                    "lowerBoundary: "   + lowerBoundary.getClass() + "\n" +
                    "resolution: " + resolution.getClass() + "\n");
        }


        if (upperBoundary instanceof Integer){
             if(!((upperBoundary.intValue()-lowerBoundary.intValue())%resolution.intValue() == 0) && (resolution.intValue() < upperBoundary.intValue())){
                 throw new IllegalArgumentException("Input values not valid. Upper bound must have a larger value than lower and resolution must be a factor of upper- and lower bound: \n" +
                         "upperBoundary: " + upperBoundary + "\n" +
                         "lowerBoundary: "   + lowerBoundary + "\n" +
                         "resolution: " + resolution + "\n");
             };
        } else if(upperBoundary instanceof Double) {
            if (!((resolution.doubleValue() > lowerBoundary.doubleValue()) && (resolution.doubleValue() < upperBoundary.doubleValue()))){
                throw new IllegalArgumentException("Input values not valid. Upper bound must have a larger value than lower and resolution must have a value lower than upper bound: \n" +
                        "upperBoundary: " + upperBoundary + "\n" +
                        "lowerBoundary: "   + lowerBoundary + "\n" +
                        "resolution: " + resolution + "\n");
            }
        } else if(upperBoundary instanceof Float) {
            if(!((resolution.floatValue() > lowerBoundary.floatValue()) && (resolution.floatValue() < upperBoundary.floatValue()))){
                throw new IllegalArgumentException("Input values not valid. Upper bound must have a larger value than lower and resolution must have a value lower than upper bound: \n" +
                        "upperBoundary: " + upperBoundary + "\n" +
                        "lowerBoundary: "   + lowerBoundary + "\n" +
                        "resolution: " + resolution + "\n");
            }
        }

    }

}
