/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic;

import java.util.UUID;

/**
 *
 * @author ancla
 */
public class BasicEvent<T> {
    private final UUID id;
    private final T value;
    
    public BasicEvent(UUID id, T value)
    {
        this.id = id;
        this.value = value;
    }

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }
    
    public T getValue() {
        return value;
    }
    
}
