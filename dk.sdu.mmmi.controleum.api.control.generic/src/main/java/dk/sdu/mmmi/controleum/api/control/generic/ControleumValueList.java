/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.control.generic;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author ancla
 */
public interface ControleumValueList extends Collection<Number>, Serializable {

    void addAll(Number[] c);

    ControleumValueList addedWith(ControleumValueList l);

    Number get(int index);

    ControleumValueList getValue(Number a, Number b);

    List<Number> getValues();
    
    List<Double> getValuesAsDouble();

    ControleumValueList multipliedWith(ControleumValueList l);

    ControleumValueList summarizedValue();

    void set(int i, Number value);
}
