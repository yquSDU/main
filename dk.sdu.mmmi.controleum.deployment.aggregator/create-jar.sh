#!/bin/bash

currentPath=$(pwd)

if ! [[ $currentPath == *"dk.sdu.mmmi.controleum.deployment.aggregator"* ]]; then
  echo "ERROR: You must be in the dk.sdu.mmmi.controleum.deployment.aggregator maven module directory before running this script"
  exit
fi


echo "##########################################"
echo "Building Controleum"
echo "##########################################"

cd .. # Navigate to the controleum root folder which contains the parent pom
mvn clean install # Build and install all the modules defined in the parent pom into the m2 folder

echo "##########################################"
echo "Creating fat JAR"
echo "##########################################"

cd dk.sdu.mmmi.controleum.deployment.aggregator
mvn clean package # Create the Controleum fat jar (Controleum class library)

echo "##########################################"
echo "Installing fat JAR into local m2 (maven) repository."
echo "GroupID: dk.sdu.mmmi"
echo "ArtifactID: controleum-library"
echo "##########################################"

mvn install:install-file \
   -Dfile=./target/Controleum-library.jar \
   -DgroupId=dk.sdu.mmmi \
   -DartifactId=controleum-library \
   -Dversion=1.1-SNAPSHOT \
   -Dpackaging=jar \
   -DgeneratePom=true


echo "##########################################"
echo "Done"
echo "##########################################"
