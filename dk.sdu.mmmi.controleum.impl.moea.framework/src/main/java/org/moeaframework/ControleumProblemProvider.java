package org.moeaframework;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControlDomainManager;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Problem;
import org.moeaframework.core.spi.ProblemProvider;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = ProblemProvider.class)
public class ControleumProblemProvider extends ProblemProvider {

    private ControlDomain problem;

    public ControleumProblemProvider() {

    }

    @Override
    public Problem getProblem(String problemName) {
        if (problemName.equalsIgnoreCase("OurProblem")
                || problemName.equalsIgnoreCase("OurProblem")) {

            ControlDomainManager cdm = Lookup.getDefault().lookup(ControlDomainManager.class);
            problem = cdm.getControlDomain(problemName) == null
                    ? cdm.createControlDomain(problemName) : cdm.getControlDomain(problemName);

            MoeaProblemAdapter moeaProblem = new MoeaProblemAdapter(problem);
            context(moeaProblem).add(Solver.class, context(problem).one(Solver.class));

            return moeaProblem;
        } else {
            return null;
        }
    }

    @Override
    public NondominatedPopulation getReferenceSet(String string) {
        return null;
    }
}
