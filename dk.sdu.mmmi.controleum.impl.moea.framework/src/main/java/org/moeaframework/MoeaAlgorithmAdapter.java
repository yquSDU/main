package org.moeaframework;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;
import dk.sdu.mmmi.controleum.api.moea.SearchStatistics;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import java.util.Date;
import java.util.List;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Variable;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;

/**
 *
 * @author jcs
 */
class MoeaAlgorithmAdapter implements Solver {

    private final Object problem;
    private Executor executor;

    public MoeaAlgorithmAdapter(Object problemContext) {
        this.problem = problemContext;

        /*this.executor = new Executor()
         .withProblemClass(DomainProblem.class, problem)
         .withAlgorithm("NSGAII")
         .withMaxEvaluations(1000);
         */
    }

    @Override
    public Solution getSolution() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SearchStatistics solve(Date t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SearchStatistics solve(Date t, ProgressHandle h, boolean useSwingThreadNotification) {

        try {
            // Read inputs
            for (Input input : context(problem).all(Input.class)) {
                input.doUpdateValue(t);
            }

            // Generate random outputs
            for (Output output : context(problem).all(Output.class)) {
                Object v = output.getRandomValue(t);
                output.setValue(v);
            }

            /*
             try {
             Evaluator.main(new String[] {
             "-p", "/Users/Newsha/Desktop/MOEA/PDF",
             "-i", "/Users/Newsha/Desktop/MOEA/PF",
             "-o", "/Users/Newsha/Desktop/MOEA/SF",
             "-a", "NSGAII",
             "-b", "OurProblem",
             "-s", "1"});
             } catch (Exception ex) {
             Exceptions.printStackTrace(ex);
             }
             */
            executor = new Executor()
                    .withProblemClass("MOEAProblemAdapter", problem)
                    .withAlgorithm("NSGAII")
                    .withMaxEvaluations(1000);

            // Run GA with Problem
            NondominatedPopulation result = executor.run();

            for (org.moeaframework.core.Solution solution : result) {
                System.out.println(solution.getObjective(0));
            }

            // Write outputs
            for (Output o : context(problem).all(Output.class)) {
                if (o.isEnabled()) {
                    int idx = context(o).one(Integer.class);
                    Variable var = result.get(0).getVariable(idx);
                }
            }
            // Return statistics

        } catch (ClassNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        }
        return new SearchStatistics(100, 100);
    }

    @Override
    public void setSearchConfiguration(SearchConfiguration configuration) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Solution> getSolutionList() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getGeneration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isTerminated() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void terminate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SearchConfiguration getSearchCfg() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

