/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.moeaframework;

import static com.decouplink.Utilities.context;
import com.google.common.collect.Lists;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Output;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jcs
 */
public class ControleumUtil {

    public static List<Concern> getObjectives(Object problem) {
        List<Concern> concerns = Lists.newArrayList();
        for (Concern concern : context(problem).all(Concern.class)) {
            if (concern.isEnabled() && concern.getPriority() != 1) {
                concerns.add(concern);
            }
        }
        return concerns;
    }

    public static List<Concern> getConstraints(Object problem) {
        List<Concern> concerns = Lists.newArrayList();
        for (Concern concern : context(problem).all(Concern.class)) {
            if (concern.isEnabled() && concern.getPriority() == 1) {
                concerns.add(concern);
            }
        }
        return concerns;
    }

    public static List<Output> getOutputs(Object problem) {

        ArrayList<Output> r = Lists.newArrayList();
        for (Output o : context(problem).all(Output.class)) {
            if (o.isEnabled()) {
                r.add(o);
            }
        }
        return r;
    }

}
