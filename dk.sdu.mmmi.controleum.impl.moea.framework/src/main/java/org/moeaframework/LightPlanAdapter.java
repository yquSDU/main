package org.moeaframework;

import dk.sdu.mmmi.controleum.impl.entities.results.LightPlan;
import dk.sdu.mmmi.controleum.impl.entities.units.Duration;
import dk.sdu.mmmi.controleum.impl.entities.units.MolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.Sample;
import dk.sdu.mmmi.controleum.impl.entities.units.Switch;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrMeter;
import dk.sdu.mmmi.controleum.impl.entities.units.UMolSqrtMeterSecond;
import dk.sdu.mmmi.controleum.impl.entities.utils.DateUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.moeaframework.core.variable.BinaryVariable;

/**
 *
 * @author ng
 */
class LightPlanAdapter extends LightPlan {

    // 24 hour binary string for a day.
    // Eg., [0,0,0...,1]
    private final BinaryVariable var;
    private final Object problem;

    LightPlanAdapter(Object problem, BinaryVariable var) {
        super(DateUtil.startOfDay(new Date()), DateUtil.endOfDay(new Date()), new Duration(DateUtil.HOUR_IN_MS));
        this.problem = problem;
        this.var = var;
    }

    @Override
    public Switch getElement(Date d) {

        int i = DateUtil.hourOfDate(d);
        boolean isOn = var.get(i);

        return isOn ? Switch.ON : Switch.OFF;
    }

    @Override
    public int size() {

        return var.getNumberOfBits();
    }

    @Override
    public List<Sample<Switch>> getList() {

        List<Sample<Switch>> r = new ArrayList<>();
        for (int i = 0; i < var.getNumberOfBits(); i++) {

            boolean isOn = var.get(i);
            Date d = DateUtil.dateOfIndex(i);

            if (isOn) {
                r.add(new Sample(d, Switch.ON));
            } else {
                r.add(new Sample(d, Switch.OFF));
            }
        }

        return r;
    }

    @Override
    public Switch getElement(int idx) {
        boolean isOn = var.get(idx);

        return isOn ? Switch.ON : Switch.OFF;
    }

    @Override
    public Date getElementStart(int idx) {
        return DateUtil.dateOfIndex(idx); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Duration getPlanDuration() {
        return new Duration(var.getNumberOfBits() * getLightInterval().toMS());
    }

    @Override
    public Switch getStatusNow() {
        boolean isOn = var.get(0);

        return isOn ? Switch.ON : Switch.OFF;
    }

    @Override
    public Duration getTotalLightOnDuration() {
        long totalMS = 0;
        for (int i = 0; i < var.getNumberOfBits(); i++) {
            Switch s = getElement(i);
            if (s.isOn()) {
                totalMS += getLightInterval().toMS();
            }
        }

        return new Duration(totalMS);
    }

    public MolSqrMeter getTotalPARSum(UMolSqrtMeterSecond lampIntensity) {
        UMolSqrMeter artPARLightSum = new UMolSqrMeter(0);

        for (int i = 0; i < var.getNumberOfBits(); i++) {
            Switch s = getElement(i);
            if (s.isOn()) {
                artPARLightSum = artPARLightSum.add(lampIntensity.times(getLightInterval()));
            }
        }
        return artPARLightSum.toMol();
    }

}
