package org.moeaframework;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Input;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.api.moea.Solution;
import dk.sdu.mmmi.controleum.api.moea.Value;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.moeaframework.core.variable.BinaryVariable;

/**
 * Adapter for Controleum solution. This allows MOEAFramework solutions to be
 * used within Controleum.
 *
 * @author jcs
 */
public class ControleumSolutionAdapter implements Solution {

    private final Object problem;
    private final org.moeaframework.core.Solution solutionAdaptee;
    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public ControleumSolutionAdapter(org.moeaframework.core.Solution solution, Object problem) {
        this.solutionAdaptee = solution;
        this.problem = problem;
    }

    @Override
    public Date getTime() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object getContext() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <T> T getValue(Class<? extends Value<T>> valueClazz) {

        Value<T> o = context(problem).one(valueClazz);

        if (o instanceof Output) {

            Output output = (Output) o;
            // Create adapter for output value
            int i = context(output).one(Integer.class);
            BinaryVariable var = (BinaryVariable) solutionAdaptee.getVariable(i);
            output.setBitSet(var.getBitSet());

            return o.getValue();
        } else {
            return context(problem).one(valueClazz).getValue();
        }

    }

    @Override
    public double getEvaluationResult(int priority) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Double> getEvaluationValues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void commit(Date now) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public dk.sdu.mmmi.controleum.api.moea.Solution crossover(dk.sdu.mmmi.controleum.api.moea.Solution b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public dk.sdu.mmmi.controleum.api.moea.Solution mutate() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public dk.sdu.mmmi.controleum.api.moea.Solution copy(Object context, Date time) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Output, Object> getOutputValues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Input, Object> getInputValues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getEvaluationValue(Class<? extends Concern> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
