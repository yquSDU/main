package org.moeaframework;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Output;
import static org.moeaframework.ControleumUtil.getConstraints;
import static org.moeaframework.ControleumUtil.getObjectives;
import static org.moeaframework.ControleumUtil.getOutputs;
import java.util.BitSet;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.BinaryVariable;

/**
 * Adapter for Controleum problems. This allows MOEA Framework {@link Problem}s
 * to be used within the Controleum library.
 *
 * @author jcs
 */
public class ControleumProblemAdapter {

    private final Problem moeaProblem;

    public ControleumProblemAdapter(Problem problem) {
        this.moeaProblem = problem;
    }

    public Problem getProblem() {
        return moeaProblem;
    }

    public Solution translate(dk.sdu.mmmi.controleum.api.moea.Solution controleumSolution) {
        Solution result = moeaProblem.newSolution();

        Object problem = controleumSolution.getContext();

        // Variables
        for (Output output : getOutputs(problem)) {
            BitSet bitSet = output.asBitSet();

            BinaryVariable moeaBinVar = new BinaryVariable(output.bitSize());
            for (int j = 0; j < output.bitSize(); j++) {
                boolean bit = bitSet.get(j);
                moeaBinVar.set(j, bit);
            }
            context(moeaBinVar).add(Output.class, output);
            Integer i = context(output).one(Integer.class);
            result.setVariable(i, moeaBinVar);
        }

        // Objectives
        int oIdx = 0;
        for (Concern concern : getObjectives(problem)) {
            result.setObjective(oIdx, controleumSolution.getEvaluationValue(concern.getClass()));
            oIdx++;
        }

        // Constraints
        int conIdx = 0;
        for (Concern concern : getConstraints(problem)) {
            result.setConstraint(conIdx, controleumSolution.getEvaluationValue(concern.getClass()));
            conIdx++;
        }

        return result;
    }
}
