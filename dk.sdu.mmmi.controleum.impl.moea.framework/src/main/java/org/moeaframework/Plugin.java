package org.moeaframework;

import com.decouplink.Disposable;
import com.decouplink.DisposableList;
import dk.sdu.mmmi.controleum.api.core.ControlDomain;
import dk.sdu.mmmi.controleum.api.core.ControleumPlugin;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author jcs
 */
@ServiceProvider(service = ControleumPlugin.class)
public class Plugin implements ControleumPlugin {

    @Override
    public Disposable create(ControlDomain g) {
        DisposableList d = new DisposableList();
        //d.add(context(g).add(Solver.class, new MOEAAlgorithmAdapter(g)));

        return d;
    }
}
