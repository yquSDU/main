package org.moeaframework;

import static com.decouplink.Utilities.context;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Output;
import static org.moeaframework.ControleumUtil.getConstraints;
import static org.moeaframework.ControleumUtil.getObjectives;
import static org.moeaframework.ControleumUtil.getOutputs;
import java.util.BitSet;
import java.util.List;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.BinaryVariable;

/**
 * Adapter for MOEAFramework problems. This allows Controleum problems to be
 * used within MOEAFramework.
 *
 * @author jcs
 */
public class MoeaProblemAdapter implements Problem {

    private final Object problem;

    public MoeaProblemAdapter(Object problem) {
        this.problem = problem;
    }

    @Override
    public String getName() {
        return "OurProblem";
    }

    @Override
    public int getNumberOfVariables() {
        return getOutputs(problem).size();
    }

    @Override
    public int getNumberOfObjectives() {
        return getObjectives(problem).size();
    }

    @Override
    public int getNumberOfConstraints() {
        return getConstraints(problem).size();
    }

    @Override
    public void evaluate(Solution solution) {

        // Map to controleum solution
        ControleumSolutionAdapter s = new ControleumSolutionAdapter(solution, problem);

        // Evaluate objectives
        int oIdx = 0;
        for (Concern concern : getObjectives(problem)) {
            solution.setObjective(oIdx, concern.evaluate(s));
            oIdx++;
        }

        int conIdx = 0;
        for (Concern concern : getConstraints(problem)) {
            solution.setConstraint(conIdx, concern.evaluate(s));
            conIdx++;
        }

    }

    @Override
    public Solution newSolution() {

        List<Concern> objectives = getObjectives(problem);
        List<Output> outputs = getOutputs(problem);
        List<Concern> constraints = getConstraints(problem);

        int numberOfObjectives = objectives.size();
        int numberOfVariables = outputs.size();
        int numberOfConstraints = constraints.size();

        Solution s = new Solution(numberOfVariables, numberOfObjectives, numberOfConstraints);

        // Set Outputs
        for (int i = 0; i < outputs.size(); i++) {

            Output output = outputs.get(i);
            context(output).add(Integer.class, i);

            BinaryVariable binVar = new BinaryVariable(output.bitSize());
            BitSet outputBitset = output.asBitSet();

            for (int j = 0; j < output.bitSize(); j++) {
                boolean bit = outputBitset.get(j);
                binVar.set(j, bit);
            }
            context(binVar).add(Output.class, output);
            s.setVariable(i, binVar);
        }
        return s;
    }

    @Override
    public void close() {
    }

}
