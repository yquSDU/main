package dk.sdu.mmmi.controleum.api.simulation;

import java.util.Date;

/**
 * @author mrj
 */
public interface SimulationContext {

    /**
     * Methods that initializes the simulation context for a specified period
     * [from:to].
     *
     * @param from
     * @param to
     */
    void init(Date from, Date to);

    /**
     * @return Number of simulation steps.
     */
    int size();

    /**
     * @return Simulation step / progress.
     */
    int position();

    /**
     * Disposal of simulation context.
     */
    void dispose();

    /**
     * @return Returns true if more simulation steps exists.
     */
    boolean hasNext();

    /**
     * @return Triggers next simulation step and return the timestamp of that
     * step.
     */
    Date next();
}
