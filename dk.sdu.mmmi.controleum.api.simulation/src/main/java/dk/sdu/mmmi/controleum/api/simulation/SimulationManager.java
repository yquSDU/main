package dk.sdu.mmmi.controleum.api.simulation;

import java.util.Date;

/**
 *
 * @author jcs
 */
public interface SimulationManager {

    void startSimulation(Date from, Date to);

    void stopSimulation();

    boolean isSimulationReady();

    boolean isSimulating();
}
