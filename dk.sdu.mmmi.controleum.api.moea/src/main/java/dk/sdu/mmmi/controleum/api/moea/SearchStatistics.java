package dk.sdu.mmmi.controleum.api.moea;

/**
 *
 * @author mrj
 */
public class SearchStatistics {

    private final int generationsEvaluated;
    private final long timeSpend;

    public SearchStatistics(int generationsEvaluated, long timeSpend) {
        this.generationsEvaluated = generationsEvaluated;
        this.timeSpend = timeSpend;
    }

    public int getGenerationsEvaluated() {
        return generationsEvaluated;
    }

    public long getTimeSpendMS() {
        return timeSpend;
    }

    @Override
    public String toString() {
        return String.format(
                "%d generations evaluated, %d ms spend.",
                getGenerationsEvaluated(),
                getTimeSpendMS());
    }
}
