package dk.sdu.mmmi.controleum.api.moea;

/**
 * @author mrj, jcs
 */
public interface Value<V> {

    public static enum Type {

        BIN, REAL
    };

    void addListener(Listener l);

    void removeListener(Listener l);

    void fireValueChanged();

    Type getType();

    V getValue();

    void setValue(V value);

    public interface Listener {

        void onValueChanged(Value v);
    }
}
