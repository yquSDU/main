package dk.sdu.mmmi.controleum.api.moea.utils;

import java.util.BitSet;

/**
 *
 * @author jcs
 */
public class BitSetUtil {

    /**
     * Encodes a double value in a <tt>BitSet</tt> by scaling the interval
     * (given by minValue and maxValue) to the co-domain of the natual number
     * representable by n bits (n=lenght).
     * <tt>minValue</tt> and <tt>maxValue</tt> are necessary to calculate a
     * scaling function between the two co-domains.
     *
     * @param bitSet the <tt>BitSet</tt> to operate on
     * @param offset the offset in the given bit set
     * @param length the number of bits used to encode <tt>value</tt>
     * @param minValue minimal value of <tt>value</tt>
     * @param maxValue maximal value of <tt>value</tt>
     * @param value the value to encode
     * @return the manipulated bit set
     * @throws RuntimeException if the value of <tt>value</tt> does not fit in
     * ]minValue, maxValue[
     * @throws RuntimeException if the number of bits (<tt>length</tt>) is
     * bigger than
     * <tt>Long.SIZE</tt>
     */
    public static BitSet doubleToBitSet(BitSet bitSet, int offset, int length, double minValue,
            double maxValue, double value) {
        if (value < minValue || value > maxValue) {
            throw new RuntimeException("Value out of bounds. "
                    + minValue + " < " + value + " < " + maxValue);
        }

        if (length > Long.SIZE) {
            throw new RuntimeException("You can not set a higher length than " + Long.SIZE
                    + " bits.");
        }

        // scaling function:
        // x element of ]minValue, maxValue[
        // ==> f(x) element of ]0, (2^(length-1)*2 -1)[
        //
        // so:
        // f(x) = (x-minValue) * m
        // m = max / (maxValue - minValue)
        double intervalLength = maxValue - minValue;
        // hint: without sign!
        int max = ((int) Math.pow(2, (length - 1))) * 2 - 1;

        // scaling the input interval to the output interval of 0...2^(length-1)*2 -1
        long scaledValue = Math.round((value - minValue) * max / intervalLength);

        // putting the scaled value to the bitset (without any sign!)
        long mask = 1;
        for (int i = 0; i < length; ++i, mask <<= 1) {
            if ((mask & scaledValue) > 0) {
                bitSet.set(offset + i);
            }
        }

        return bitSet;
    }

    /**
     * Extracts a double value from <tt>bitSet</tt> from the offset position to
     * the given offset+length-1. It does the reverse scaling done by
     * <tt>public static BitSet doubleToBitSet(BitSet bitSet, int offset, int
     * length, double minValue, double maxValue, double value)</tt>.
     *
     * @param bitSet the bit set to operate on
     * @param offset the offset posistion in the bit set
     * @param length the number of bit that represent the value
     * @param minValue the same value set in <tt>doubleToBitSet(...)</tt>
     * @param maxValue the same value set in <tt>doubleToBitSet(...)</tt>
     * @return the back scaled value from the bit set
     * @see public static BitSet doubleToBitSet(BitSet bitSet, int offset, int
     * length, double minValue, double maxValue, double value)
     * @throws RuntimeException if the number of bits given by <tt>length</tt>
     * is greater than Long.SIZE
     */
    public static double bitSetToDouble(BitSet bitSet, int offset, int length, double minValue,
            double maxValue) {
        // checking the bit length
        if (length > Long.SIZE) {
            throw new RuntimeException("You can not set a higher length than " + Long.SIZE
                    + " bits.");
        }

        // getting the value as natural number from the bitSet
        long longValue = 0;
        long mask = 1;
        for (int i = 0; i < length; ++i, mask <<= 1) {
            if (bitSet.get(offset + i)) {
                longValue |= mask;
            }
        }

        // hint: without sign!
        long max = ((long) Math.pow(2.0, (length - 1))) * 2 - 1;

        // returning the scaled back double value
        return (longValue / (max / (maxValue - minValue)) + minValue);
    }
}
