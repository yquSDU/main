package dk.sdu.mmmi.controleum.api.moea;

import java.util.Date;
import java.util.List;
import org.netbeans.api.progress.ProgressHandle;

/**
 *
 * @author jcs
 */
public interface Solver {

    Solution getSolution();

    List<Solution> getSolutionList();

    SearchStatistics solve(Date t);

    SearchStatistics solve(Date t, ProgressHandle h, boolean useSwingThreadNotification);

    void setSearchConfiguration(SearchConfiguration configuration);

    SearchConfiguration getSearchCfg();

    int getGeneration();

    boolean isTerminated();

    void terminate();
}
