/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.moea;

/**
 *
 * @author Anders
 */
public interface IssueObserver<V> {
    void update(V value);
}
