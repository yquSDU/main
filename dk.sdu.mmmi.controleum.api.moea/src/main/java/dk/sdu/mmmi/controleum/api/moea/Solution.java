package dk.sdu.mmmi.controleum.api.moea;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author mrj & jcs
 */
public interface Solution {

    /**
     * @return Solution time
     */
    Date getTime();

    /**
     * Commit is called after the end of an epoch, i.e. when the search has
     * terminated
     *
     * @param now Time of commit
     */
    void commit(Date now);

    /**
     * @param priority The priority
     * @return Total evaluation sum of all concerns with the given priority
     */
    double getEvaluationResult(int priority);

    /**
     * @return Context object of the solution
     */
    Object getContext();

    /**
     * @param valueClazz The class of value type
     * @return The value of the class type
     */
    <T> T getValue(Class<? extends Value<T>> valueClazz);

    /**
     * @return A list of concern evaluation values.
     */
    List<Double> getEvaluationValues();

    /**
     * @param corncern The concern
     * @return The evaluation value of the given concern
     */
    double getEvaluationValue(Class<? extends Concern> corncern);

    /**
     * @param solutionB The solution to be crossover by this solution
     * @return The product of SolutionB crossover with this solution
     */
    Solution crossover(Solution solutionB);

    /**
     * @return Mutation of this solution
     */
    Solution mutate();

    /**
     * @param context Solution context
     * @param time Solution time
     * @return Copy of this solution
     */
    Solution copy(Object context, Date time);

    /**
     * @return A map of outputType/value
     */
    Map<Output, Object> getOutputValues();

    /**
     * @return A map of inputType/value
     */
    Map<Input, Object> getInputValues();

    /**
     * @return A map of issueType/value
     */
    Map<Issue, Object> getIssueValues();
    
    /**
     * @param <T>
     * @param valueObject
     * @return Value of a specific value object in the option.
     */
    <T> T getValue(Value<T> valueObject);

}
