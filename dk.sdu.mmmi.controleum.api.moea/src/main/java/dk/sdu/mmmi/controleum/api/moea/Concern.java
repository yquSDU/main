package dk.sdu.mmmi.controleum.api.moea;

import java.util.Map;

/**
 * @author mrj, jcs
 */
public interface Concern extends Element, Commitable {

    public final static int LOW_PRIORITY = 10;

    public final static int HIGH_PRIORITY = 1;

    // Reserved for hard concerns
    public final static int HARD_PRIORITY = 0;

    void setPriority(int priority, boolean fireEvent);

    int getPriority();

    /**
     * Should concern be evaluated during negotiation?
     */
    boolean isEnabled();

    /**
     * Configure whether concern should be evaluated during negotiation.
     */
    void setEnabled(boolean enabled);

    /**
     * Configure whether concern should be evaluated during negotiation.
     */
    void setEnabled(boolean enabled, boolean fireEvent);

    /**
     * Was the last negotiated solution optimal for this concern?
     */
    boolean isOptimal();

    /**
     * Returns this concerns evaluation result of the last negotiated solution.
     * Zero is optimal (i.e. minimization).
     */
    double getEvaluationResult();

    /**
     * Override to provide Concern- and Solution-specific help for individual
     * Values.
     *
     * @return Maps from Values to help Strings.
     */
    Map<Class, String> getValueHelp(Solution s);

    /**
     * Get all Value-specific help Strings.
     *
     * @return Map that maps Value type to help text.
     */
    Map<Class, String> getValueHelp();

    /**
     * Get help String for a given Value.
     */
    String getValueHelpOrNull(Class<? extends Value> v);

    /**
     * Override this to add an evaluation function. An evaluation function with
     * Concern.HARD_PRIORITY that returns 0 (true) or 1 (false) represents a
     * hard requirements. Other evaluate functions with lower priorities
     * represent soft requirements.
     *
     * @param option Option to be evaluated
     * @return Evaluation of the option (set of issues). sBy convention the
     * issues in each option are minimized. For example, a low price is better
     * when evaluating price issues.
     */
    double evaluate(Solution option);

    /**
     *
     * @param s
     */
    @Override
    void commit(Solution s);

    void fireConcernChanged();
}
