package dk.sdu.mmmi.controleum.api.moea;

import java.util.Date;

/**
 * @author mrj, jcs
 */
public interface Input<V> extends Value<V>, Element {

    void doUpdateValue(Date t);
}
