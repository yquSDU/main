package dk.sdu.mmmi.controleum.api.moea.ancla;

import java.util.List;

public interface IPopulationSelector {

    /**
     * Creates a new pareto front based on the current pareto front and the new population.
     * @param paretoFront
     * @param newPopulation
     * @return Returns a new pareto front.
     */
    boolean selectParetoFront(List<ISolution> paretoFront, List<ISolution> newPopulation);
}
