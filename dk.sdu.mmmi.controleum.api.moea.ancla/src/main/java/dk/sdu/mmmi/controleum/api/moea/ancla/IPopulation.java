/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dk.sdu.mmmi.controleum.api.moea.ancla;


import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.SearchConfiguration;;

import java.util.List;

/**
 *
 * @author ancla
 */
public interface IPopulation {

    boolean isEvolved();
    
    IPopulation evolve();

    List<Double> getBestFitness();

    ISolution getBestSolution();

    double getFitnessVariance();

    List<Double> getMeanFitness();

    ISolution getMeanSolution();

    int getSize();

    List<Double> getWorstFitness();

    ISolution getWorstSolution();
    
    List<ISolution> getPopulation();

    SearchConfiguration getSearchConfiguration();

    List<Concern> getConcerns();

    IParetoFront getFront();
    
    ISolution getFairSolution();
    
    ISolution getMaxSocialBenefitSolution();

}
