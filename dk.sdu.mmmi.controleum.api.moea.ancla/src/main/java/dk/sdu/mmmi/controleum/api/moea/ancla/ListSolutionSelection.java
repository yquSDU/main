package dk.sdu.mmmi.controleum.api.moea.ancla;

import java.util.List;

/**
 *
 * @author aiu
 */
public interface ListSolutionSelection {
    public List<ISolution> select(List<ISolution> pop);
}
