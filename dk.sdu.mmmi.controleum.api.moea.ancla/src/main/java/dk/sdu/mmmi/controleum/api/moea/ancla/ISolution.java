package dk.sdu.mmmi.controleum.api.moea.ancla;

import dk.sdu.mmmi.controleum.api.control.generic.ControleumValueList;
import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.Issue;
import dk.sdu.mmmi.controleum.api.moea.Output;
import dk.sdu.mmmi.controleum.api.moea.Solution;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author ancla
 */
public interface ISolution extends Solution {
    //TODO Proper generics
    //ControleumValueList getValue(UUID id);

    Map<Concern, Double> getFitnessValuesMap();

    double getSummarizedEvaluationResult(int priority, Map<Concern, MinMax> concernsMinMax);

    List<Concern> getConcerns();

    double getRootEvaluationResult(List<Concern> ca);
    
    boolean isValid();
    
    void invalidate();
    
    void calculateFitness();

    void publishContract(Date now);

    Map<String, Issue> getIssues();

    double getApproxFairnessEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getFairnessAnalysisEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getQuantitativeFairnessEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getEntropyEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getSummarizedEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getEgalitarianEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getElitistEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getRankEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    double getNashEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);

    List<Double> getLexiMinEvaluationResult(List<Double> score,Map<Concern, MinMax> minMaxFitnessValues);
}
