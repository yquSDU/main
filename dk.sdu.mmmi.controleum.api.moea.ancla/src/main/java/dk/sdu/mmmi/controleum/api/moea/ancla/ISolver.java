package dk.sdu.mmmi.controleum.api.moea.ancla;

import dk.sdu.mmmi.controleum.api.moea.SearchStatistics;
import dk.sdu.mmmi.controleum.api.moea.Solver;
import java.util.Date;

/**
 *
 * @author ancla
 */
public interface ISolver extends Solver {

    public IPopulation getCurrentPopulation();
    
    public long getConvergeTime();
    
    public int getConvergenGen();
    
    public void dispose();
    
    public void doEvolution();

}
