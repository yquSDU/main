package dk.sdu.mmmi.controleum.api.moea.ancla;

import java.util.List;

/**
 *
 * @author ancla
 */
public interface ISolutionSelector {
    public ISolution getSolution(List<ISolution> pop);
    
}
