package dk.sdu.mmmi.controleum.api.moea.ancla;

import java.util.List;

public interface IParetoFront {

    /**
     * Add list of solutions to the pareto front.
     * Solutions are only added if they dominate existing solutions.
     * @param newPopulation
     * @return Returns true if any solution is added to the pareto front.
     */
    boolean addAll(List<ISolution> newPopulation);

    /**
     * Add a solution to the pareto front.
     * The solution is only added if it dominate existing solutions.
     * @param newSolution
     * @return Returns true if the solution is added to the pareto front.
     */
    boolean add(ISolution newSolution);

    /**
     * Creates and returns a copy of the pareto front.
     * The front is sorted by best first.
     * @return Returns a copy of the pareto front
     */
    List<ISolution> getCopy();
}
