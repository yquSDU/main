package dk.sdu.mmmi.controleum.api.core;

/**
 *
 * @author jcs
 */
public interface ControleumListener {

    void onControlDomainUpdated(ControleumEvent e);

    void onControlDomainAdded(ControleumEvent e);

    void onControlDomainDeleted(ControleumEvent e);
}
