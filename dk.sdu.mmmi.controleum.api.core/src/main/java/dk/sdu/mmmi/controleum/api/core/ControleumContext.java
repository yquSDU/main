package dk.sdu.mmmi.controleum.api.core;

import java.io.Serializable;

/**
 * ControlDomain is an independently extensible context.
 *
 * @author mrj, jcs
 */
public interface ControleumContext extends Serializable {

    int getID();

    void setName(String name);

    String getName();
}
