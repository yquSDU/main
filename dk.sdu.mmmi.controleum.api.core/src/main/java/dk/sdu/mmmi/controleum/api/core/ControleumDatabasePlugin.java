/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.api.core;

import com.decouplink.Disposable;
import com.decouplink.Shareable;

/**
 * This interface corresponds to the ControleumPluging. However, implementations
 * of this interface will be instantiated before ControleumPlugin, and they 
 * cannot be added to a Context at runtime.
 * @author ancla
 */
@Shareable
public interface ControleumDatabasePlugin {
    Disposable create(ControleumContext domain);
}
