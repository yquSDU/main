/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.solutionselectors;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class ApproximatedFairnessSelectorI implements ISolutionSelector {

 
   @Override
    public ISolution getSolution(List<ISolution> pop) {
        Map<Concern, MinMax> minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(pop);
        double[] bestApproximateFairness = new double[10];
        java.util.Arrays.fill(bestApproximateFairness, Double.MAX_VALUE);
        ISolution finalSolution = null;
       
        for (ISolution solution : pop) {
           
            Map<Concern, Double> normalizedSolution = NormalizationUtil.NormalizeSolution(solution, minMaxFitnessValues);
            //Prio specific
            double[] approximateFairnessArray = new double[10];
            for (int i = 0; i < 10; i++) {
                Map<Concern, Double> concernsForPrio = new HashMap<>();
                for (Concern c : normalizedSolution.keySet()) {
                    if (c.getPriority() == i) {
                        concernsForPrio.put(c, normalizedSolution.get(c));
                    }
                }
                double averageCost = NormalizationUtil.CalculateAverageCost(concernsForPrio);

                double approximateFairness = 0.0;
                for (Double cost : concernsForPrio.values()) {
                   if(concernsForPrio.size()>1)  // check for number of concerns in a priority group
                       approximateFairness += Math.pow(cost - averageCost, 2) / concernsForPrio.size();
                   else
                       approximateFairness=cost;
                   
                
                }
                approximateFairnessArray[i] = approximateFairness;
                
            }
            
            for (int i = 0; i < 10; i++) {
                if (approximateFairnessArray[i] < bestApproximateFairness[i]) {
                    bestApproximateFairness = approximateFairnessArray;
                    finalSolution = solution;
                    
                    
                }
                else if (approximateFairnessArray[i] > bestApproximateFairness[i])
                {
                    break;
                }
            }
            
        }
        
        return finalSolution;
    }

   

}
