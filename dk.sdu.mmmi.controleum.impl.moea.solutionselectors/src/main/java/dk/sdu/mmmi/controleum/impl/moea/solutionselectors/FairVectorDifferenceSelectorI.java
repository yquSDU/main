/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.solutionselectors;

import dk.sdu.mmmi.controleum.api.moea.Concern;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolution;
import dk.sdu.mmmi.controleum.api.moea.ancla.MinMax;
import dk.sdu.mmmi.controleum.api.moea.ancla.ISolutionSelector;
import dk.sdu.mmmi.controleum.impl.moea.comperator.NormalizedVarianceComparator;
import dk.sdu.mmmi.controleum.impl.moea.comperator.NormalizedVectorComparator;
import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ancla
 */
public class FairVectorDifferenceSelectorI implements ISolutionSelector {

    @Override
    public ISolution getSolution(List<ISolution> pop) {
        //Currently only supports fairness across entire population. Hence, should only be used when no priorities are present.
        //Recipe: Get a list of sorted solutions with their associated vector length.
        Map<Concern, MinMax> minMaxFitnessValues = NormalizationUtil.GetMinMaxFitnessValues(pop);
        NormalizedVectorComparator BY_NORMALIZED_VECTOR = new NormalizedVectorComparator(minMaxFitnessValues);
        Collections.sort(pop, BY_NORMALIZED_VECTOR);
        //Create then a sublist containing all solutions with the lowest vector norm (if more have the same).
        List<ISolution> popOptimalVector = new ArrayList<>();
        ISolution bestVector = pop.get(0);
        popOptimalVector.add(bestVector);
        for (ISolution s : pop) {
            if (NormalizationUtil.VectorLength(bestVector, minMaxFitnessValues) == NormalizationUtil.VectorLength(s, minMaxFitnessValues)) {
                popOptimalVector.add(s);
            } else {
                break;
            }
        }
        NormalizedVarianceComparator BY_NORMALIZED_VARIANCE = new NormalizedVarianceComparator(minMaxFitnessValues);
        Collections.sort(popOptimalVector, BY_NORMALIZED_VARIANCE);
        return (ISolution) popOptimalVector.get(0);
    }

}
