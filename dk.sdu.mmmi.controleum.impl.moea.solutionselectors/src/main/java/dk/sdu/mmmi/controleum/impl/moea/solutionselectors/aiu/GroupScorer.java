package dk.sdu.mmmi.controleum.impl.moea.solutionselectors.aiu;

import dk.sdu.mmmi.controleum.impl.moea.utils.NormalizationUtil;
import dk.sdu.mmmi.controleum.api.moea.graph.IGroupScorerInterface;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author aiu
 */
public enum GroupScorer implements IGroupScorerInterface {

    SOCIAL {
        @Override
        public double score(List<Double> normalizedValues) {
            double result = 0.0;

            for (Double d : normalizedValues) {     //qqq get the normalized sum of all concerns in the same vertex
                result = result + d;
            }
            return result;

        }

    },
    NASH_PRODUCT {
        @Override
        public double score(List<Double> normalizedValues) {
            double result = 1.0;

            for (Double d : normalizedValues) {
                result *= (d + 1.0);
            }
            return result;

        }

    },
    MEDIAN_RANK {
        @Override
        public double score(List<Double> normalizedValues) {
            double result = 0.0;
            int n = normalizedValues.size();
            if (n > 0) {
                int k = ((n % 2 == 0) ? n / 2 : (n + 1) / 2); // calculating k for median rank dictator
                Collections.sort(normalizedValues, Collections.reverseOrder()); // descending ordering of cost vector
                result = normalizedValues.get(k - 1); //returning kth cost
            }

            return result;

        }

    },
    APPROX_FAIRNESS {
        @Override
        public double score(List<Double> normalizedValues) {
            double result = 0.0;
            
            if (normalizedValues.size() > 1) {
                double average = NormalizationUtil.CalculateAverageCost(normalizedValues);
                for (Double d : normalizedValues) {
                    result += Math.pow((average - d), 2) / (double)normalizedValues.size();
                }
            } else {
                result = normalizedValues.get(0);
            }
            return result;

        }

    },
    EGALITARIAN {
        @Override
        public double score(List<Double> normalizedValues) {

            double maxCost = 0.0;
            if (normalizedValues.size() > 1) {
                for (Double d : normalizedValues) {
                    maxCost = (d > maxCost) ? d : maxCost;
                }
            } else {
                maxCost = normalizedValues.get(0);
            }
            return maxCost;

        }
    },
    ELITIST {
        @Override
        public double score(List<Double> normalizedValues) {
            double minCost = Double.MAX_VALUE;
            if (normalizedValues.size() > 1) {
                for (Double d : normalizedValues) {
                    minCost = (d < minCost) ? d : minCost;
                }
            } else {
                minCost = normalizedValues.get(0);
            }
            return minCost;

        }
    },
    QUANTITATIVE_FAIRNESS {
        @Override
        public double score(List<Double> normalizedValues) {
            double result = 0.0;
            double sum = 0.0;
            double squareOfSum = 0.0;
            double sumOfSquare = 0.0;
            double N = normalizedValues.size();

            if (normalizedValues.size() > 1) {
                for (Double d : normalizedValues) {
                    sum += d + 1.0;
                }
                squareOfSum = Math.pow(sum, 2);

                for (Double d : normalizedValues) {
                    sumOfSquare += Math.pow((d + 1.0), 2);
                }

                result = (1.0 - (squareOfSum / (N * sumOfSquare)));

            } else {
                result = normalizedValues.get(0);
            }

            return result;

        }
    },
    
    ENTROPY {
        @Override
        public double score(List<Double> normalizedValues) {
            double result = 0.0;
            double sum = 0.0;
            double entropy=0.0;
            double proportion = 0.0;

            if (normalizedValues.size() > 1) {
                for (Double d : normalizedValues) {
                    sum += d + 1.0;
                }
                

                for (Double d : normalizedValues) {
                    proportion = (d + 1.0)/sum;
                    entropy+=proportion*((Math.log(proportion))/(Math.log(2.0)));
                }

                result = Math.abs(1000-Math.abs(entropy));

            } else {
                result = normalizedValues.get(0);
            }

            return result;

        }
    },
    FAIRNESS_ANALYSIS { // minimize standard deviation
        @Override
        public double score(List<Double> normalizedValues) {
            double result = 0.0;
            double variance = 0.0;
            double average = NormalizationUtil.CalculateAverageCost(normalizedValues);
            if (normalizedValues.size() > 1) {
                for (Double d : normalizedValues) {
                    variance += Math.pow((average - d), 2);
                }
                result = Math.sqrt(variance / normalizedValues.size());
            } else {
                result = normalizedValues.get(0);
            }
            return result;
        }
    },;

    public abstract double score(List<Double> normalizedValues);

}
