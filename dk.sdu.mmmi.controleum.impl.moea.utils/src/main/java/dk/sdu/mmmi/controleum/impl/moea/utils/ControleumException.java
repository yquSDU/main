/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.sdu.mmmi.controleum.impl.moea.utils;

/**
 *
 * @author ancla
 */
public class ControleumException extends Exception {
    public ControleumException(String message)
    {
        super(message);
    }
}
